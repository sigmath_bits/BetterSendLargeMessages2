//META{"name":"BetterSendLargeMessages2","displayName":"BetterSendLargeMessages2","website":"https://gitlab.com/sigmath6/BetterSendLargeMessages2","source":"https://gitlab.com/sigmath6/BetterSendLargeMessages2/raw/master/release/BetterSendLargeMessages2.plugin.js"}*//
/*@cc_on
@if (@_jscript)
	
	// Offer to self-install for clueless users that try to run this directly.
	var shell = WScript.CreateObject("WScript.Shell");
	var fs = new ActiveXObject("Scripting.FileSystemObject");
	var pathPlugins = shell.ExpandEnvironmentStrings("%APPDATA%\BetterDiscord\plugins");
	var pathSelf = WScript.ScriptFullName;
	// Put the user at ease by addressing them in the first person
	shell.Popup("It looks like you've mistakenly tried to run me directly. \n(Don't do that!)", 0, "I'm a plugin for BetterDiscord", 0x30);
	if (fs.GetParentFolderName(pathSelf) === fs.GetAbsolutePathName(pathPlugins)) {
		shell.Popup("I'm in the correct folder already.", 0, "I'm already installed", 0x40);
	} else if (!fs.FolderExists(pathPlugins)) {
		shell.Popup("I can't find the BetterDiscord plugins folder.\nAre you sure it's even installed?", 0, "Can't install myself", 0x10);
	} else if (shell.Popup("Should I copy myself to BetterDiscord's plugins folder for you?", 0, "Do you need some help?", 0x34) === 6) {
		fs.CopyFile(pathSelf, fs.BuildPath(pathPlugins, fs.GetFileName(pathSelf)), true);
		// Show the user where to put plugins in the future
		shell.Exec("explorer " + pathPlugins);
		shell.Popup("I'm installed!", 0, "Successfully installed", 0x40);
	}
	WScript.Quit();

@else@*/

var BetterSendLargeMessages2 = (() => {
    const config = {"main":"index.js","info":{"name":"BetterSendLargeMessages2","authors":[{"name":"Sigmath Bits"}],"version":"2.8.14","description":"Better Send Large Messages version 2. Opens a popout when your message is too large, which allows you to automatically sending the message as several smaller messages. Message splitting is avoided in the middle of paragraphs and sentences.","github":"https://gitlab.com/sigmath6/BetterSendLargeMessages2","github_raw":"https://gitlab.com/sigmath6/BetterSendLargeMessages2/raw/master/release/BetterSendLargeMessages2.plugin.js"},"changelog":[{"title":"Fixes","type":"fixes","items":["Fixed bug when editing messages and opening dialogue","Fixed potential bug with clicking away to close dialogue"]}],"defaultConfig":[{"id":"main_settings","type":"category","name":"Better Send Large Messages Settings","shown":true,"settings":[{"id":"restore_message","type":"dropdown","value":"always","options":[{"label":"Always","value":"always"},{"label":"Never","value":"never"},{"label":"For short messages","value":"short"}]},{"id":"lock_by_default","type":"switch","value":false},{"id":"note_on_right","type":"switch","value":false},{"id":"reduced_toasts","type":"switch","value":false}]},{"id":"shortcuts","type":"category","name":"Send Large Message Shortcuts","settings":[{"id":"open_modal_shortcut","type":"keybind","value":[162,77]},{"id":"lock_message_shortcut","type":"keybind","value":[162,76]},{"id":"clear_message_shortcut","type":"keybind","value":[162,46]}]}],"strings":{"en":{"description":"Better Send Large Messages version 2. Opens a popout when your message is too large, which allows you to automatically sending the message as several smaller messages. Message splitting is avoided in the middle of paragraphs and sentences.","toast_allsent_text":"All messages sent!","toast_restored_message_info":"Original message restored.","toast_message_locked_info":"Saved message locked!","toast_message_unlocked_info":"Saved message unlocked!","toast_locked_message_warning":"Saved message locked! Can't open Send Large Message dialogue!","toast_deleted_message_warning":"Saved message deleted!","toast_cleared_message_warning":"Message cleared!","toast_undeleted_message_info":"Message restored!","toast_cannot_delete_warning":"Cannot delete locked message!","modal_messages_translation":"Messages","modal_messages_warning":"Do not send too many messages!","modal_load_warning":"Loaded matching saved message!","modal_overwrite_warning":"Last message overwritten!","modal_header_text":"Send Large Message","btn_cancel_text":"Cancel","btn_send_text":"Send","note_tooltip":"Open message","note_tooltip_locked":"Open message [locked]","settings":{"main_settings":{"name":"Send Large Messages Settings","restore_message":{"name":"Restore Message","note":"Whether or not to restore your message to the chatbox when you close the Send Large Message dialogue, trimmed by message limit.","options":{"always":"Always","never":"Never","short":"For short messages"}},"lock_by_default":{"name":"Lock Messages by Default","note":"Lock saved messages by default to prevent them getting overwritten or deleted."},"note_on_right":{"name":"Show Icon on Right","note":"Show saved message icon on the right."},"reduced_toasts":{"name":"Reduce Notifications","note":"Reduces the number of toast notifications."}},"shortcuts":{"name":"Send Large Message Shortcuts","open_modal_shortcut":{"name":"Send Message Shortcut","note":"Shortcut to open the Send Large Message dialogue."},"lock_message_shortcut":{"name":"Lock Message Shortcut","note":"Shortcut to lock the current saved message. Locking a saved message means it cannot be overwritten or deleted and will not restore to the chatbox. Ctrl + Clicking the saved message icon will also toggle locking."},"clear_message_shortcut":{"name":"Clear Message Shortcut","note":"Shortcut to clear the Send Large Message dialogue or delete the existing saved message. Press again to restore the message."}}}},"pl":{"description":"Lepsze wysyłanie dużych wiadomości w wersji 2. Otwiera wyskakujące okienko, gdy wiadomość jest zbyt duża, co pozwala automatycznie wysyłać wiadomość jako kilka mniejszych wiadomości. W środkach akapitów i zdań unika się dzielenia wiadomości.","toast_allsent_text":"Wzystkie wiadomości wysłane!","toast_restored_message_info":"Pierwotna wiadomość odtworzona.","toast_message_locked_info":"Zapisana wiadomość zablokowana!","toast_message_unlocked_info":"Zapisana wiadomość odblokowana!","toast_locked_message_warning":"Zapisana wiadomość zablokowana! Nie można otworzyć okna rozszerzenia!","toast_deleted_message_warning":"Zapisana wiadomość usunięta!","toast_cleared_message_warning":"Wiadomość wyczyszczona!","toast_undeleted_message_info":"Wiadomość odtworzona!","toast_cannot_delete_warning":"Nie można usunąć zablokowanej wiadomości!","modal_messages_translation":"Wiadomości","modal_messages_warning":"Nie wysyłaj zbyt wielu wiadomości!","modal_load_warning":"Pasująca zapisana wiadomość załadowana!","modal_overwrite_warning":"Ostatnia wiadomość nadpisana!","modal_header_text":"Wyślij Dużą Wiadomość","btn_cancel_text":"Anuluj","btn_send_text":"Wyślij","note_tooltip":"Otwórz wiadomość","note_tooltip_locked":"Otwórz wiadomość [zablokowany]","settings":{"main_settings":{"name":"Ustawienia Rozszerzeń","restore_message":{"name":"Odtwórz Wiadomość","note":"Czy odtworzyć twoją wiadomość w chatboksie gdy zamkniesz okno rozszerzenia (przycięte do limitu znaków wiadomości).","options":{"always":"Zawsze","never":"Nigdy","short":"W przypadku krótkich wiadomości"}},"lock_by_default":{"name":"Domyślnie Blokuj Wiadomości","note":"Domyślnie blokuj zapisane wiadomości aby zapobiec ich nadpisaniu lub usunięciu."},"note_on_right":{"name":"Uwaga Ikona po Prawej Stronie","note":"Pokaż ikonę zapisanej wiadomości po prawej stronie."},"reduced_toasts":{"name":"Ogranicz Powiadomienia","note":"Ogranicza liczbę powiadomień."}},"shortcuts":{"name":"Skróty Rozszerszeń","open_modal_shortcut":{"name":"Skrót do Wysyłania Wiadomości","note":"Skrót, który otwiera okno rozszerzenia."},"lock_message_shortcut":{"name":"Skrót do Blokowania Wiadomości","note":"Skrót, który blokuje aktualnie zapisaną wiadomość. Blokowanie zapisanej wiadomości oznacza, że nie może ona zostać nadpisana, usunięta lub przywrócona do chatboksa. Ctrl + kliknięcie na ikonę zapisanej wiadomości również spowoduje jej zablokowanie."},"clear_message_shortcut":{"name":"Skrót do Czyszczenia Wiadomości","note":"Skrót, który wyczyści okno rozszerzenia lub usunie istniejącą zapisaną wiadomość. Wciśnij ponownie aby przywrócić wiadomość."}}}},"zh":{"description":"更好的发送大消息版本2.当您的消息太大时打开弹出窗口，这允许您自动将消息作为几个较小的消息发送。在段落和句子的中间避免了消息拆分。","toast_allsent_text":"发送所有消息！","toast_restored_message_info":"原始邮件已恢复。","toast_message_locked_info":"保存的消息已锁定！","toast_message_unlocked_info":"保存的消息已解锁！","toast_locked_message_warning":"保存的消息已锁定！无法打开发送大消息对话框！","toast_deleted_message_warning":"保存的消息已删除！","toast_cleared_message_warning":"消息已清除！","toast_undeleted_message_info":"消息恢复！","toast_cannot_delete_warning":"无法删除锁定的消息！","modal_messages_translation":"消息","modal_messages_warning":"不要发送太多消息！","modal_load_warning":"加载匹配保存的消息！","modal_overwrite_warning":"最后一条消息被覆盖！","modal_header_text":"发送大邮件","btn_cancel_text":"取消","btn_send_text":"发送","note_tooltip":"打开消息","note_tooltip_locked":"打开消息[锁定]","settings":{"main_settings":{"name":"发送大邮件设置","restore_message":{"name":"还原消息","note":"关闭“发送大邮件”对话框时是否将邮件恢复到聊天框，按邮件限制进行修剪。","options":{"always":"总是","never":"决不","short":"对于短信"}},"lock_by_default":{"name":"默认锁定消息","note":"默认情况下锁定已保存的邮件以防止它们被覆盖或删除。"},"note_on_right":{"name":"在右侧显示图标","note":"在右侧显示已保存的消息图标。"},"reduced_toasts":{"name":"减少通知","note":"减少吐司通知的数量。"}},"shortcuts":{"name":"发送大邮件快捷方式","open_modal_shortcut":{"name":"发送消息快捷方式","note":"打开发送大消息对话框的快捷方式。"},"lock_message_shortcut":{"name":"锁定消息快捷方式","note":"锁定当前保存的消息的快捷方式。锁定已保存的消息意味着它不能被覆盖或删除，也不会恢复到聊天框。 Ctrl +单击保存的消息图标也将切换锁定。"},"clear_message_shortcut":{"name":"清除消息快捷方式","note":"清除“发送大邮件”对话框或删除现有已保存邮件的快捷方式。再按一次可恢复信息。"}}}},"zh-CN":{"description":"更好的发送大消息版本2.当您的消息太大时打开弹出窗口，这允许您自动将消息作为几个较小的消息发送。在段落和句子的中间避免了消息拆分。","toast_allsent_text":"发送所有消息！","toast_restored_message_info":"原始邮件已恢复。","toast_message_locked_info":"保存的消息已锁定！","toast_message_unlocked_info":"保存的消息已解锁！","toast_locked_message_warning":"保存的消息已锁定！无法打开发送大消息对话框！","toast_deleted_message_warning":"保存的消息已删除！","toast_cleared_message_warning":"消息已清除！","toast_undeleted_message_info":"消息恢复！","toast_cannot_delete_warning":"无法删除锁定的消息！","modal_messages_translation":"消息","modal_messages_warning":"不要发送太多消息！","modal_load_warning":"加载匹配保存的消息！","modal_overwrite_warning":"最后一条消息被覆盖！","modal_header_text":"发送大邮件","btn_cancel_text":"取消","btn_send_text":"发送","note_tooltip":"打开消息","note_tooltip_locked":"打开消息[锁定]","settings":{"main_settings":{"name":"发送大邮件设置","restore_message":{"name":"还原消息","note":"关闭“发送大邮件”对话框时是否将邮件恢复到聊天框，按邮件限制进行修剪。","options":{"always":"总是","never":"决不","short":"对于短信"}},"lock_by_default":{"name":"默认锁定消息","note":"默认情况下锁定已保存的邮件以防止它们被覆盖或删除。"},"note_on_right":{"name":"在右侧显示图标","note":"在右侧显示已保存的消息图标。"},"reduced_toasts":{"name":"减少通知","note":"减少吐司通知的数量。"}},"shortcuts":{"name":"发送大邮件快捷方式","open_modal_shortcut":{"name":"发送消息快捷方式","note":"打开发送大消息对话框的快捷方式。"},"lock_message_shortcut":{"name":"锁定消息快捷方式","note":"锁定当前保存的消息的快捷方式。锁定已保存的消息意味着它不能被覆盖或删除，也不会恢复到聊天框。 Ctrl +单击保存的消息图标也将切换锁定。"},"clear_message_shortcut":{"name":"清除消息快捷方式","note":"清除“发送大邮件”对话框或删除现有已保存邮件的快捷方式。再按一次可恢复信息。"}}}},"zh-TW":{"description":"更好的發送大消息版本2.當您的消息太大時打開彈出窗口，這允許您自動將消息作為幾個較小的消息發送。在段落和句子的中間避免了消息拆分。","toast_allsent_text":"發送所有消息！","toast_restored_message_info":"原始郵件已恢復。","toast_message_locked_info":"保存的消息已鎖定！","toast_message_unlocked_info":"保存的消息已解鎖！","toast_locked_message_warning":"保存的消息已鎖定！無法打開發送大消息對話框！","toast_deleted_message_warning":"保存的消息已刪除！","toast_cleared_message_warning":"消息已清除！","toast_undeleted_message_info":"消息恢復！","toast_cannot_delete_warning":"無法刪除鎖定的消息！","modal_messages_translation":"消息","modal_messages_warning":"不要發送太多消息！","modal_load_warning":"加載匹配保存的消息！","modal_overwrite_warning":"最後一條消息被覆蓋！","modal_header_text":"發送大郵件","btn_cancel_text":"取消","btn_send_text":"發送","note_tooltip":"打開消息","note_tooltip_locked":"打開消息[鎖定]","settings":{"main_settings":{"name":"發送大郵件設置","restore_message":{"name":"還原消息","note":"關閉“發送大郵件”對話框時是否將郵件恢復到聊天框，按郵件限制進行修剪。","options":{"always":"總是","never":"決不","short":"對於短信"}},"lock_by_default":{"name":"默認鎖定消息","note":"默認情況下鎖定已保存的郵件以防止它們被覆蓋或刪除。"},"note_on_right":{"name":"在右側顯示圖標","note":"在右側顯示已保存的消息圖標。"},"reduced_toasts":{"name":"減少通知","note":"減少吐司通知的數量。"}},"shortcuts":{"name":"發送大郵件快捷方式","open_modal_shortcut":{"name":"發送消息快捷方式","note":"打開發送大消息對話框的快捷方式。"},"lock_message_shortcut":{"name":"鎖定消息快捷方式","note":"鎖定當前保存的消息的快捷方式。鎖定已保存的消息意味著它不能被覆蓋或刪除，也不會恢復到聊天框。 Ctrl +單擊保存的消息圖標也將切換鎖定。"},"clear_message_shortcut":{"name":"清除消息快捷方式","note":"清除“發送大郵件”對話框或刪除現有已保存郵件的快捷方式。再按一次可恢復信息。"}}}},"cs":{"description":"Lepší odesílání velkých zpráv verze 2. Pokud je zpráva příliš velká, otevře vyskakovací okno, což vám umožní automaticky odeslat zprávu jako několik menších zpráv. Rozdělení zpráv je vyloučeno uprostřed odstavců a vět.","toast_allsent_text":"Všechny zprávy byly odeslány!","toast_restored_message_info":"Původní zpráva byla obnovena.","toast_message_locked_info":"Uložená zpráva uzamčena!","toast_message_unlocked_info":"Uložená zpráva odemčena!","toast_locked_message_warning":"Uložená zpráva uzamčena! Dialog Odeslat velkou zprávu nelze otevřít!","toast_deleted_message_warning":"Uložená zpráva byla smazána!","toast_cleared_message_warning":"Zpráva byla vymazána!","toast_undeleted_message_info":"Zpráva byla obnovena!","toast_cannot_delete_warning":"Uzamčenou zprávu nelze smazat!","modal_messages_translation":"Zprávy","modal_messages_warning":"Neposílejte příliš mnoho zpráv!","modal_load_warning":"Načteno odpovídající uložená zpráva!","modal_overwrite_warning":"Poslední zpráva byla přepsána!","modal_header_text":"Odeslat velkou zprávu","btn_cancel_text":"zrušení","btn_send_text":"Poslat","note_tooltip":"Otevřít zprávu","note_tooltip_locked":"Otevřít zprávu [zamknuto]","settings":{"main_settings":{"name":"Odeslat nastavení velkých zpráv","restore_message":{"name":"Obnovit zprávu","note":"Zda se má nebo nemá obnovit vaše zpráva do chatboxu, když zavřete dialogové okno Odeslat velkou zprávu, oříznuté limitem zprávy.","options":{"always":"Vždy","never":"Nikdy","short":"Pro krátké zprávy"}},"lock_by_default":{"name":"Uzamknout zprávy ve výchozím nastavení","note":"Uložené zprávy jsou ve výchozím nastavení uzamčeny, aby se zabránilo jejich přepsání nebo odstranění."},"note_on_right":{"name":"Zobrazit ikonu vpravo","note":"Zobrazit ikonu uložené zprávy vpravo."},"reduced_toasts":{"name":"Omezte oznámení","note":"Snižuje počet oznámení o přípitku."}},"shortcuts":{"name":"Odeslat velké zkratky pro zprávy","open_modal_shortcut":{"name":"Odeslat zkratku zprávy","note":"Zkratka pro otevření dialogu Odeslat velkou zprávu."},"lock_message_shortcut":{"name":"Zkratka zkratky zprávy","note":"Zkratka pro uzamčení aktuálně uložené zprávy. Uzamknutí uložené zprávy znamená, že ji nelze přepsat nebo smazat a neobnoví se do chatboxu. Ctrl + Kliknutím na ikonu uložené zprávy také zamknete uzamčení."},"clear_message_shortcut":{"name":"Vymazat zástupce","note":"Zkratka pro vymazání dialogu Odeslat velkou zprávu nebo odstranění existující uložené zprávy. Znovu stiskněte pro obnovení zprávy."}}}},"da":{"description":"Bedre send store meddelelser version 2. Åbner en pop-up, når din besked er for stor, hvilket giver dig mulighed for automatisk at sende beskeden som flere mindre beskeder. Opdeling af meddelelser undgås midt i afsnit og sætninger.","toast_allsent_text":"Alle meddelelser sendt!","toast_restored_message_info":"Originalmeddelelse gendannet.","toast_message_locked_info":"Gemt besked låst!","toast_message_unlocked_info":"Gemt besked låst op!","toast_locked_message_warning":"Gemt besked låst! Kan ikke åbne Send Large Message-dialog!","toast_deleted_message_warning":"Gemt besked er slettet!","toast_cleared_message_warning":"Beskeden ryddet!","toast_undeleted_message_info":"Meddelelse gendannet!","toast_cannot_delete_warning":"Kan ikke slette den låste besked!","modal_messages_translation":"Beskeder","modal_messages_warning":"Send ikke for mange beskeder!","modal_load_warning":"Indlæst matchende gemt besked!","modal_overwrite_warning":"Sidste besked overskrevet!","modal_header_text":"Send stor besked","btn_cancel_text":"Afbestille","btn_send_text":"Sende","note_tooltip":"Åben besked","note_tooltip_locked":"Åben besked [låst]","settings":{"main_settings":{"name":"Send indstillinger for store meddelelser","restore_message":{"name":"Gendan meddelelse","note":"Uanset om du vil gendanne din besked til chatboksen, når du lukker dialogboksen Send stor besked, beskåret efter beskedgrænse.","options":{"always":"Altid","never":"Aldrig","short":"Til korte beskeder"}},"lock_by_default":{"name":"Lås meddelelser som standard","note":"Lås gemte meddelelser som standard for at forhindre, at de overskrives eller slettes."},"note_on_right":{"name":"Vis ikon til højre","note":"Vis gemt meddelelsesikon til højre."},"reduced_toasts":{"name":"Reducer underretninger","note":"Reducerer antallet af toast-meddelelser."}},"shortcuts":{"name":"Send store meddelelsesgenveje","open_modal_shortcut":{"name":"Send beskedgenvej","note":"Genvej til åbning af dialogboksen Send stor besked."},"lock_message_shortcut":{"name":"Lås genvej til besked","note":"Genvej til at låse den aktuelle gemte meddelelse. At låse en gemt besked betyder, at den ikke kan overskrives eller slettes og gendannes ikke til chatboksen. Ctrl + Klik på det gemte meddelelsesikon for at skifte låsning."},"clear_message_shortcut":{"name":"Ryd meddelelsesgenvej","note":"Genvej til at rydde dialogboksen Send stor besked eller slette den eksisterende gemte meddelelse. Tryk igen for at gendanne meddelelsen."}}}},"nl":{"description":"Beter grote berichten verzenden versie 2. Opent een pop-up wanneer uw bericht te groot is, waarmee u het bericht automatisch als meerdere kleinere berichten kunt verzenden. Het splitsen van berichten wordt vermeden in het midden van alinea's en zinnen.","toast_allsent_text":"Alle berichten verzonden!","toast_restored_message_info":"Origineel bericht hersteld.","toast_message_locked_info":"Opgeslagen bericht vergrendeld!","toast_message_unlocked_info":"Opgeslagen bericht ontgrendeld!","toast_locked_message_warning":"Opgeslagen bericht vergrendeld! Kan dialoogvenster Groot bericht verzenden niet openen!","toast_deleted_message_warning":"Opgeslagen bericht verwijderd!","toast_cleared_message_warning":"Bericht gewist!","toast_undeleted_message_info":"Bericht hersteld!","toast_cannot_delete_warning":"Kan vergrendeld bericht niet verwijderen!","modal_messages_translation":"berichten","modal_messages_warning":"Stuur niet teveel berichten!","modal_load_warning":"Overeenkomend opgeslagen bericht geladen!","modal_overwrite_warning":"Laatste bericht overschreven!","modal_header_text":"Verzend groot bericht","btn_cancel_text":"annuleren","btn_send_text":"Sturen","note_tooltip":"Open bericht","note_tooltip_locked":"Bericht openen [vergrendeld]","settings":{"main_settings":{"name":"Instellingen voor grote berichten verzenden","restore_message":{"name":"Bericht herstellen","note":"Of je bericht al dan niet moet worden hersteld in de chatbox wanneer je het dialoogvenster Groot bericht verzenden sluit, ingekort op berichtlimiet.","options":{"always":"Altijd","never":"Nooit","short":"Voor korte berichten"}},"lock_by_default":{"name":"Standaard berichten vergrendelen","note":"Blokkeer opgeslagen berichten standaard om te voorkomen dat ze worden overschreven of verwijderd."},"note_on_right":{"name":"Pictogram rechts weergeven","note":"Toon het opgeslagen berichtpictogram aan de rechterkant."},"reduced_toasts":{"name":"Verminder meldingen","note":"Vermindert het aantal toastmeldingen."}},"shortcuts":{"name":"Verzend snelkoppelingen naar grote berichten","open_modal_shortcut":{"name":"Verzend bericht snelkoppeling","note":"Snelkoppeling om het dialoogvenster Groot bericht verzenden te openen."},"lock_message_shortcut":{"name":"Vergrendel bericht snelkoppeling","note":"Snelkoppeling om het huidige opgeslagen bericht te vergrendelen. Een opgeslagen bericht vergrendelen betekent dat het niet kan worden overschreven of verwijderd en niet wordt hersteld naar de chatbox. Ctrl + klikken op het pictogram voor opgeslagen berichten schakelt ook tussen vergrendeling."},"clear_message_shortcut":{"name":"Berichtsnelkoppeling wissen","note":"Snelkoppeling om het dialoogvenster Groot bericht verzenden te wissen of het bestaande opgeslagen bericht te verwijderen. Druk opnieuw om het bericht te herstellen."}}}},"fr":{"description":"Mieux envoyer des messages volumineux version 2. Ouvre une fenêtre contextuelle lorsque votre message est trop volumineux, ce qui vous permet de l'envoyer automatiquement sous la forme de plusieurs messages plus petits. La division des messages est évitée au milieu des paragraphes et des phrases.","toast_allsent_text":"Tous les messages envoyés!","toast_restored_message_info":"Message original restauré.","toast_message_locked_info":"Message enregistré verrouillé!","toast_message_unlocked_info":"Message enregistré débloqué!","toast_locked_message_warning":"Message enregistré verrouillé! Impossible d'ouvrir le dialogue Envoyer un message volumineux!","toast_deleted_message_warning":"Message enregistré supprimé!","toast_cleared_message_warning":"Message effacé!","toast_undeleted_message_info":"Message restauré!","toast_cannot_delete_warning":"Impossible de supprimer le message verrouillé!","modal_messages_translation":"messages","modal_messages_warning":"N'envoyez pas trop de messages!","modal_load_warning":"Chargé correspondant au message sauvegardé!","modal_overwrite_warning":"Dernier message écrasé!","modal_header_text":"Envoyer un message volumineux","btn_cancel_text":"Annuler","btn_send_text":"Envoyer","note_tooltip":"Message ouvert","note_tooltip_locked":"Message ouvert [verrouillé]","settings":{"main_settings":{"name":"Paramètres d'envoi de messages volumineux","restore_message":{"name":"Message de restauration","note":"Restaure ou non votre message dans la boîte de discussion lorsque vous fermez la boîte de dialogue Envoyer un message volumineux, limitée par le nombre maximal de messages.","options":{"always":"Toujours","never":"Jamais","short":"Pour les messages courts"}},"lock_by_default":{"name":"Verrouiller les messages par défaut","note":"Verrouillez les messages sauvegardés par défaut pour éviter qu’ils ne soient écrasés ou supprimés."},"note_on_right":{"name":"Afficher l'icône à droite","note":"Afficher l’icône du message enregistré à droite."},"reduced_toasts":{"name":"Réduire les notifications","note":"Réduit le nombre de notifications de pain grillé."}},"shortcuts":{"name":"Envoyer des raccourcis de message volumineux","open_modal_shortcut":{"name":"Envoyer un raccourci de message","note":"Raccourci pour ouvrir la boîte de dialogue Envoyer un message volumineux."},"lock_message_shortcut":{"name":"Verrouiller le raccourci de message","note":"Raccourci pour verrouiller le message enregistré actuel. Le verrouillage d'un message enregistré signifie qu'il ne peut pas être écrasé ou supprimé et ne sera pas restauré dans la boîte de discussion. Ctrl + Clic sur l'icône du message enregistré va également activer le verrouillage."},"clear_message_shortcut":{"name":"Effacer le raccourci de message","note":"Raccourci pour effacer la boîte de dialogue Envoyer un message volumineux ou supprimer le message enregistré existant. Appuyez à nouveau pour restaurer le message."}}}},"de":{"description":"Besseres Senden großer Nachrichten Version 2. Öffnet ein Popout, wenn Ihre Nachricht zu groß ist, sodass Sie die Nachricht automatisch als mehrere kleinere Nachrichten senden können. Das Aufteilen von Nachrichten in der Mitte von Absätzen und Sätzen wird vermieden.","toast_allsent_text":"Alle Nachrichten gesendet!","toast_restored_message_info":"Originalnachricht wiederhergestellt.","toast_message_locked_info":"Gespeicherte Nachricht gesperrt!","toast_message_unlocked_info":"Gespeicherte Nachricht freigeschaltet!","toast_locked_message_warning":"Gespeicherte Nachricht gesperrt! Das Dialogfeld \"Große Nachricht senden\" kann nicht geöffnet werden.","toast_deleted_message_warning":"Gespeicherte Nachricht gelöscht!","toast_cleared_message_warning":"Nachricht gelöscht!","toast_undeleted_message_info":"Nachricht wiederhergestellt!","toast_cannot_delete_warning":"Gesperrte Nachricht kann nicht gelöscht werden!","modal_messages_translation":"Mitteilungen","modal_messages_warning":"Senden Sie nicht zu viele Nachrichten!","modal_load_warning":"Geladene passende gespeicherte Nachricht!","modal_overwrite_warning":"Letzte Nachricht überschrieben!","modal_header_text":"Große Nachricht senden","btn_cancel_text":"Stornieren","btn_send_text":"Senden","note_tooltip":"Nachricht öffnen","note_tooltip_locked":"Nachricht öffnen [gesperrt]","settings":{"main_settings":{"name":"Einstellungen für das Senden großer Nachrichten","restore_message":{"name":"Nachricht wiederherstellen","note":"Gibt an, ob Ihre Nachricht in der Chatbox wiederhergestellt werden soll, wenn Sie das Dialogfeld \"Große Nachricht senden\" schließen, das durch das Nachrichtenlimit begrenzt ist.","options":{"always":"Immer","never":"noch nie","short":"Für kurze Nachrichten"}},"lock_by_default":{"name":"Nachrichten standardmäßig sperren","note":"Sperren Sie gespeicherte Nachrichten standardmäßig, um zu verhindern, dass sie überschrieben oder gelöscht werden."},"note_on_right":{"name":"Symbol rechts anzeigen","note":"Zeigt das gespeicherte Nachrichtensymbol auf der rechten Seite an."},"reduced_toasts":{"name":"Benachrichtigungen reduzieren","note":"Reduziert die Anzahl der Toastbenachrichtigungen."}},"shortcuts":{"name":"Shortcuts für große Nachrichten senden","open_modal_shortcut":{"name":"Verknüpfung zum Senden von Nachrichten","note":"Verknüpfung zum Öffnen des Dialogfelds \"Große Nachricht senden\"."},"lock_message_shortcut":{"name":"Nachrichtenverknüpfung sperren","note":"Verknüpfung, um die aktuell gespeicherte Nachricht zu sperren. Das Sperren einer gespeicherten Nachricht bedeutet, dass sie nicht überschrieben oder gelöscht werden kann und nicht in der Chatbox wiederhergestellt wird. Strg + Klicken auf das Symbol für gespeicherte Nachrichten schaltet die Sperre ebenfalls um."},"clear_message_shortcut":{"name":"Löschen Sie die Nachrichtenkombination","note":"Verknüpfung zum Löschen des Dialogfelds \"Große Nachricht senden\" oder zum Löschen der vorhandenen gespeicherten Nachricht. Drücken Sie erneut, um die Nachricht wiederherzustellen."}}}},"el":{"description":"Έκδοση για καλύτερη αποστολή μεγάλων μηνυμάτων 2. Ανοίγει ένα αναδυόμενο παράθυρο όταν το μήνυμά σας είναι πολύ μεγάλο, πράγμα που σας επιτρέπει να στέλνετε αυτόματα το μήνυμα ως μερικά μικρότερα μηνύματα. Ο διαχωρισμός μηνυμάτων αποφεύγεται στη μέση των παραγράφων και των προτάσεων.","toast_allsent_text":"Όλα τα μηνύματα αποστέλλονται!","toast_restored_message_info":"Αρχικό μήνυμα αποκαταστάθηκε.","toast_message_locked_info":"Το αποθηκευμένο μήνυμα είναι κλειδωμένο!","toast_message_unlocked_info":"Αποθηκευμένο μήνυμα ξεκλειδωμένο!","toast_locked_message_warning":"Το αποθηκευμένο μήνυμα είναι κλειδωμένο! Δεν είναι δυνατή η εμφάνιση του διαλόγου \"Αποστολή μεγάλου μηνύματος\".","toast_deleted_message_warning":"Το αποθηκευμένο μήνυμα διαγράφηκε!","toast_cleared_message_warning":"Μήνυμα εκκαθαρισμένο!","toast_undeleted_message_info":"Μήνυμα έχει αποκατασταθεί!","toast_cannot_delete_warning":"Δεν είναι δυνατή η διαγραφή κλειδωμένου μηνύματος!","modal_messages_translation":"Μηνύματα","modal_messages_warning":"Μην στείλετε πάρα πολλά μηνύματα!","modal_load_warning":"Το φορτωμένο αποθηκευμένο μήνυμα που ταιριάζει!","modal_overwrite_warning":"Το τελευταίο μήνυμα έχει επικαλυφθεί!","modal_header_text":"Αποστολή μεγάλου μηνύματος","btn_cancel_text":"Ματαίωση","btn_send_text":"Στείλετε","note_tooltip":"Ανοίξτε το μήνυμα","note_tooltip_locked":"Άνοιγμα μηνύματος [κλειδωμένο]","settings":{"main_settings":{"name":"Αποστολή ρυθμίσεων μεγάλων μηνυμάτων","restore_message":{"name":"Επαναφορά μηνύματος","note":"Είτε θέλετε να επαναφέρετε το μήνυμά σας στο chatbox όταν κλείσετε το παράθυρο αποστολής μηνυμάτων μεγάλης διάρκειας, αλλάζοντας το όριο μηνυμάτων.","options":{"always":"Πάντα","never":"Ποτέ","short":"Για σύντομα μηνύματα"}},"lock_by_default":{"name":"Κλείδωμα μηνυμάτων από προεπιλογή","note":"Κλείστε αποθηκευμένα μηνύματα από προεπιλογή για να αποφευχθεί η επικύρωση ή η διαγραφή τους."},"note_on_right":{"name":"Εμφάνιση εικονιδίου στα δεξιά","note":"Εμφάνιση του εικονιδίου αποθηκευμένου μηνύματος στα δεξιά."},"reduced_toasts":{"name":"Μειώστε τις ειδοποιήσεις","note":"Μειώνει τον αριθμό των ειδοποιήσεων τοστ."}},"shortcuts":{"name":"Αποστολή σύντομων μηνυμάτων συντόμευσης","open_modal_shortcut":{"name":"Αποστολή σύντομων μηνυμάτων","note":"Συντόμευση για να ανοίξετε το παράθυρο \"Αποστολή μεγάλου μηνύματος\"."},"lock_message_shortcut":{"name":"Κλείδωμα συντόμευσης μηνυμάτων","note":"Συντόμευση για να κλειδώσετε το τρέχον αποθηκευμένο μήνυμα. Το κλείδωμα ενός αποθηκευμένου μηνύματος σημαίνει ότι δεν μπορεί να αντικατασταθεί ή να διαγραφεί και δεν θα αποκατασταθεί στο chatbox. Ctrl + Το κλικ στο εικονίδιο του αποθηκευμένου μηνύματος θα αλλάξει επίσης το κλείδωμα."},"clear_message_shortcut":{"name":"Διαγραφή συντόμευσης μηνυμάτων","note":"Συντόμευση για να καταργήσετε το παράθυρο \"Αποστολή μεγάλου μηνύματος\" ή να διαγράψετε το υπάρχον αποθηκευμένο μήνυμα. Πατήστε ξανά για να επαναφέρετε το μήνυμα."}}}},"hu":{"description":"Jobb nagy méretű üzenetek küldése 2. verzió. Megnyit egy előugró ablakot, ha az üzenet túl nagy, amely lehetővé teszi az üzenet automatikus elküldését több kisebb üzenetként. Az üzenetek felosztása kerülhető a bekezdések és a mondatok közepén.","toast_allsent_text":"Minden üzenet elküldve!","toast_restored_message_info":"Az eredeti üzenet visszaállítva.","toast_message_locked_info":"A mentett üzenet zárolva van!","toast_message_unlocked_info":"A mentett üzenet nyitva van!","toast_locked_message_warning":"A mentett üzenet zárolva van! Nem nyitható meg a Nagy üzenet küldése párbeszédpanel!","toast_deleted_message_warning":"A mentett üzenet törölve!","toast_cleared_message_warning":"Üzenet törölve!","toast_undeleted_message_info":"Üzenet visszaállítva!","toast_cannot_delete_warning":"Nem lehet törölni a lezárt üzenetet!","modal_messages_translation":"üzenetek","modal_messages_warning":"Ne küldjön túl sok üzenetet!","modal_load_warning":"Betölti a megfelelő mentett üzenetet!","modal_overwrite_warning":"Utolsó üzenet felülírva!","modal_header_text":"Nagy üzenet küldése","btn_cancel_text":"Megszünteti","btn_send_text":"Elküld","note_tooltip":"Nyissa meg az üzenetet","note_tooltip_locked":"Üzenet megnyitása [lezárva]","settings":{"main_settings":{"name":"Küldje el a Nagy üzenetek beállításait","restore_message":{"name":"Üzenet visszaállítása","note":"Annak eldöntése, hogy visszaállítja-e az üzenetet a chat-fiókba, amikor bezárja a Nagy üzenet küldése párbeszédpanelt, az üzenetkorlát korlátozva.","options":{"always":"Mindig","never":"Soha","short":"Rövid üzenetekhez"}},"lock_by_default":{"name":"Üzenetek zárolása alapértelmezés szerint","note":"Alapértelmezés szerint zárolja a mentett üzeneteket, hogy ne kerüljenek felülírásra vagy törlésre."},"note_on_right":{"name":"Ikon megjelenítése jobb oldalon","note":"Mutassa az elmentett üzenet ikont a jobb oldalon."},"reduced_toasts":{"name":"Csökkentse az értesítéseket","note":"Csökkenti a pirítós értesítések számát."}},"shortcuts":{"name":"Nagy üzenet-parancsikonok küldése","open_modal_shortcut":{"name":"Üzenet küldése parancsikon","note":"Parancsikon a Nagy üzenet küldése párbeszédpanel megnyitásához."},"lock_message_shortcut":{"name":"Zárolási üzenet parancsikonja","note":"Parancsikon az aktuálisan elmentett üzenet lezárásához. A mentett üzenet zárolása azt jelenti, hogy azt nem lehet felülírni vagy törölni, és nem áll vissza a csevegõdobozba. Ctrl + A mentett üzenet ikonjára kattintva a zárolást is válthatja."},"clear_message_shortcut":{"name":"Üzenet parancsikon törlése","note":"Parancsikon a Nagy üzenet küldése párbeszédpanel törléséhez vagy a meglévő mentett üzenet törléséhez. Az üzenet visszaállításához nyomja meg újra."}}}},"it":{"description":"Meglio inviare messaggi di grandi dimensioni versione 2. Apre un popup quando il messaggio è troppo grande, il che consente di inviare automaticamente il messaggio come più piccoli messaggi. La divisione dei messaggi è evitata nel mezzo di paragrafi e frasi.","toast_allsent_text":"Tutti i messaggi inviati!","toast_restored_message_info":"Messaggio originale ripristinato.","toast_message_locked_info":"Messaggio salvato bloccato!","toast_message_unlocked_info":"Messaggio salvato sbloccato!","toast_locked_message_warning":"Messaggio salvato bloccato! Impossibile aprire la finestra di dialogo Invia messaggio di grandi dimensioni!","toast_deleted_message_warning":"Messaggio salvato cancellato!","toast_cleared_message_warning":"Messaggio cancellato!","toast_undeleted_message_info":"Messaggio ripristinato!","toast_cannot_delete_warning":"Impossibile eliminare il messaggio bloccato!","modal_messages_translation":"messaggi","modal_messages_warning":"Non inviare troppi messaggi!","modal_load_warning":"Messaggio salvato corrispondente corrispondente caricato!","modal_overwrite_warning":"Ultimo messaggio sovrascritto!","modal_header_text":"Invia un messaggio di grandi dimensioni","btn_cancel_text":"Annulla","btn_send_text":"Inviare","note_tooltip":"Messaggio aperto","note_tooltip_locked":"Apri messaggio [bloccato]","settings":{"main_settings":{"name":"Invia impostazioni messaggi di grandi dimensioni","restore_message":{"name":"Ripristina messaggio","note":"Se ripristinare o meno il messaggio nella chat quando si chiude la finestra di dialogo Invia messaggio di grandi dimensioni, ridotta dal limite del messaggio.","options":{"always":"Sempre","never":"Mai","short":"Per brevi messaggi"}},"lock_by_default":{"name":"Blocca messaggi per impostazione predefinita","note":"Blocca i messaggi salvati per impostazione predefinita per evitare che vengano sovrascritti o eliminati."},"note_on_right":{"name":"Mostra icona a destra","note":"Mostra l'icona del messaggio salvato sulla destra."},"reduced_toasts":{"name":"Riduci le notifiche","note":"Riduce il numero di notifiche toast."}},"shortcuts":{"name":"Invia scorciatoie per messaggi di grandi dimensioni","open_modal_shortcut":{"name":"Invia collegamento a un messaggio","note":"Scorciatoia per aprire la finestra di dialogo Invia messaggio di grandi dimensioni."},"lock_message_shortcut":{"name":"Collegamento rapido al messaggio","note":"Scorciatoia per bloccare il messaggio attualmente salvato. Il blocco di un messaggio salvato significa che non può essere sovrascritto o eliminato e non verrà ripristinato nella chat. Ctrl + Facendo clic sull'icona del messaggio salvato si attiva anche il blocco."},"clear_message_shortcut":{"name":"Cancella scorciatoia messaggio","note":"Scorciatoia per cancellare la finestra di dialogo Invia messaggio di grandi dimensioni o eliminare il messaggio salvato esistente. Premere di nuovo per ripristinare il messaggio."}}}},"ja":{"description":"Better Send Large Messagesバージョン2。メッセージが大きすぎるときにポップアウトを開きます。これにより、メッセージをいくつかの小さいメッセージとして自動的に送信できます。メッセージの分割は、段落と文の途中で回避されます。","toast_allsent_text":"すべてのメッセージが送信されました！","toast_restored_message_info":"元のメッセージが復元されました。","toast_message_locked_info":"保存されたメッセージはロックされています！","toast_message_unlocked_info":"保存されたメッセージのロックが解除されました！","toast_locked_message_warning":"保存されたメッセージはロックされています！ [大きなメッセージを送信]ダイアログを開くことができません！","toast_deleted_message_warning":"保存したメッセージを削除しました！","toast_cleared_message_warning":"メッセージをクリアしました！","toast_undeleted_message_info":"メッセージが復元されました！","toast_cannot_delete_warning":"ロックされたメッセージを削除できません！","modal_messages_translation":"メッセージ","modal_messages_warning":"あまりにも多くのメッセージを送信しないでください！","modal_load_warning":"一致する保存済みメッセージをロードしました！","modal_overwrite_warning":"最後のメッセージが上書きされました！","modal_header_text":"大きなメッセージを送信","btn_cancel_text":"キャンセル","btn_send_text":"送る","note_tooltip":"メッセージを開く","note_tooltip_locked":"メッセージを開く[ロック]","settings":{"main_settings":{"name":"大きなメッセージの送信設定","restore_message":{"name":"復元メッセージ","note":"[メッセージの制限]でトリミングされた[大きなメッセージの送信]ダイアログを閉じるときに、チャットボックスにメッセージを復元するかどうか。","options":{"always":"常に","never":"決して","short":"ショートメッセージ用"}},"lock_by_default":{"name":"デフォルトでメッセージをロックする","note":"デフォルトで保存されたメッセージをロックして、上書きまたは削除されないようにします。"},"note_on_right":{"name":"右にアイコンを表示","note":"右側に保存済みメッセージアイコンを表示します。"},"reduced_toasts":{"name":"通知を減らす","note":"トースト通知の数を減らします。"}},"shortcuts":{"name":"大きなメッセージのショートカットを送信する","open_modal_shortcut":{"name":"メッセージのショートカットを送信","note":"[大きなメッセージを送信]ダイアログを開くためのショートカット。"},"lock_message_shortcut":{"name":"ロックメッセージのショートカット","note":"現在保存されているメッセージをロックするショートカット。保存されたメッセージをロックすると、上書きまたは削除できず、チャットボックスに復元されません。 Ctrl +保存されたメッセージアイコンをクリックすると、ロックも切り替わります。"},"clear_message_shortcut":{"name":"メッセージのショートカットをクリア","note":"[大きなメッセージの送信]ダイアログをクリアするか、既存の保存済みメッセージを削除するショートカット。もう一度押すと、メッセージが復元されます。"}}}},"ko":{"description":"더 큰 메시지 보내기 버전 2. 메시지가 너무 클 때 팝업을 열어서 여러 개의 작은 메시지로 메시지를 자동으로 보낼 수 있습니다. 단락 및 문장 중간에 메시지 분할이 방지됩니다.","toast_allsent_text":"모든 메시지를 보냈습니다!","toast_restored_message_info":"원본 메시지가 복원되었습니다.","toast_message_locked_info":"저장된 메시지가 잠겼습니다!","toast_message_unlocked_info":"저장된 메시지가 잠금 해제되었습니다!","toast_locked_message_warning":"저장된 메시지가 잠겼습니다! 큰 메시지 보내기 대화 상자를 열 수 없습니다!","toast_deleted_message_warning":"저장된 메시지가 삭제되었습니다!","toast_cleared_message_warning":"메시지가 지워졌습니다!","toast_undeleted_message_info":"메시지가 복원되었습니다!","toast_cannot_delete_warning":"잠긴 메시지를 삭제할 수 없습니다!","modal_messages_translation":"메시지","modal_messages_warning":"너무 많은 메시지를 보내지 마십시오!","modal_load_warning":"일치하는 저장된 메시지를로드했습니다!","modal_overwrite_warning":"마지막 메시지를 덮어 썼습니다!","modal_header_text":"큰 메시지 보내기","btn_cancel_text":"취소","btn_send_text":"보내다","note_tooltip":"메시지 열기","note_tooltip_locked":"메시지 열기 [잠김]","settings":{"main_settings":{"name":"큰 메시지 보내기 설정","restore_message":{"name":"메시지 복원","note":"큰 메시지 보내기 대화 상자를 닫을 때 메시지를 대화 상자로 복원할지 여부를 메시지 제한으로 정리합니다.","options":{"always":"항상","never":"못","short":"짧은 메시지"}},"lock_by_default":{"name":"기본적으로 메시지 잠금","note":"저장된 메시지를 덮어 쓰거나 삭제하지 않도록 기본적으로 저장된 메시지를 잠급니다."},"note_on_right":{"name":"오른쪽에 아이콘 표시","note":"오른쪽에 저장된 메시지 아이콘을 표시하십시오."},"reduced_toasts":{"name":"알림 감소","note":"토스트 알림 수를 줄입니다."}},"shortcuts":{"name":"큰 메시지 바로 가기 보내기","open_modal_shortcut":{"name":"메시지 보내기 바로 가기","note":"큰 메시지 보내기 대화 상자를 여는 바로 가기입니다."},"lock_message_shortcut":{"name":"메시지 바로 가기 잠금","note":"현재 저장된 메시지를 잠그는 바로 가기 저장된 메시지를 잠그면 메시지를 덮어 쓰거나 삭제할 수 없으며 대화 상자로 복원되지 않습니다. Ctrl + 저장된 메시지 아이콘을 클릭하면 잠금이 토글됩니다."},"clear_message_shortcut":{"name":"명확한 메시지 바로 가기","note":"큰 메시지 보내기 대화 상자를 지우거나 기존에 저장된 메시지를 삭제하는 바로 가기입니다. 다시 누르면 메시지가 복원됩니다."}}}},"pt":{"description":"Better Send Large Messages versão 2. Abre uma janela pop-up quando sua mensagem é muito grande, o que permite enviar automaticamente a mensagem como várias mensagens menores. A divisão de mensagens é evitada no meio de parágrafos e frases.","toast_allsent_text":"Todas as mensagens enviadas!","toast_restored_message_info":"Mensagem original restaurada.","toast_message_locked_info":"Mensagem salva bloqueada!","toast_message_unlocked_info":"Mensagem salva desbloqueada!","toast_locked_message_warning":"Mensagem salva bloqueada! Não é possível abrir o diálogo Enviar mensagem grande!","toast_deleted_message_warning":"Mensagem salva excluída!","toast_cleared_message_warning":"Mensagem apagada!","toast_undeleted_message_info":"Mensagem restaurada!","toast_cannot_delete_warning":"Não é possível excluir a mensagem bloqueada!","modal_messages_translation":"Mensagens","modal_messages_warning":"Não envie muitas mensagens!","modal_load_warning":"Mensagem salva correspondente carregada!","modal_overwrite_warning":"Última mensagem substituída!","modal_header_text":"Enviar mensagem grande","btn_cancel_text":"Cancelar","btn_send_text":"Mandar","note_tooltip":"Mensagem aberta","note_tooltip_locked":"Mensagem aberta [bloqueada]","settings":{"main_settings":{"name":"Configurações de envio de mensagens grandes","restore_message":{"name":"Restaurar mensagem","note":"Se você deve ou não restaurar sua mensagem na caixa de bate-papo ao fechar a caixa de diálogo Enviar mensagem grande, aparada pelo limite de mensagens.","options":{"always":"Sempre","never":"Nunca","short":"Para mensagens curtas"}},"lock_by_default":{"name":"Bloquear mensagens por padrão","note":"Bloqueie as mensagens salvas por padrão para evitar que sejam substituídas ou excluídas."},"note_on_right":{"name":"Mostrar ícone à direita","note":"Mostrar o ícone da mensagem salva à direita."},"reduced_toasts":{"name":"Reduzir Notificações","note":"Reduz o número de notificações do sistema."}},"shortcuts":{"name":"Enviar atalhos para mensagens grandes","open_modal_shortcut":{"name":"Enviar Mensagem Atalho","note":"Atalho para abrir a caixa de diálogo Enviar mensagem grande."},"lock_message_shortcut":{"name":"Atalho de mensagem de bloqueio","note":"Atalho para bloquear a mensagem salva atualmente. Bloquear uma mensagem salva significa que ela não pode ser substituída ou excluída e não será restaurada na caixa de bate-papo. Ctrl + Clicar no ícone da mensagem salva também alternará o bloqueio."},"clear_message_shortcut":{"name":"Limpar atalho de mensagem","note":"Atalho para limpar a caixa de diálogo Enviar mensagem grande ou excluir a mensagem salva existente. Pressione novamente para restaurar a mensagem."}}}},"pt-PT":{"description":"Better Send Large Messages versão 2. Abre uma janela pop-up quando sua mensagem é muito grande, o que permite enviar automaticamente a mensagem como várias mensagens menores. A divisão de mensagens é evitada no meio de parágrafos e frases.","toast_allsent_text":"Todas as mensagens enviadas!","toast_restored_message_info":"Mensagem original restaurada.","toast_message_locked_info":"Mensagem salva bloqueada!","toast_message_unlocked_info":"Mensagem salva desbloqueada!","toast_locked_message_warning":"Mensagem salva bloqueada! Não é possível abrir o diálogo Enviar mensagem grande!","toast_deleted_message_warning":"Mensagem salva excluída!","toast_cleared_message_warning":"Mensagem apagada!","toast_undeleted_message_info":"Mensagem restaurada!","toast_cannot_delete_warning":"Não é possível excluir a mensagem bloqueada!","modal_messages_translation":"Mensagens","modal_messages_warning":"Não envie muitas mensagens!","modal_load_warning":"Mensagem salva correspondente carregada!","modal_overwrite_warning":"Última mensagem substituída!","modal_header_text":"Enviar mensagem grande","btn_cancel_text":"Cancelar","btn_send_text":"Mandar","note_tooltip":"Mensagem aberta","note_tooltip_locked":"Mensagem aberta [bloqueada]","settings":{"main_settings":{"name":"Configurações de envio de mensagens grandes","restore_message":{"name":"Restaurar mensagem","note":"Se você deve ou não restaurar sua mensagem na caixa de bate-papo ao fechar a caixa de diálogo Enviar mensagem grande, aparada pelo limite de mensagens.","options":{"always":"Sempre","never":"Nunca","short":"Para mensagens curtas"}},"lock_by_default":{"name":"Bloquear mensagens por padrão","note":"Bloqueie as mensagens salvas por padrão para evitar que sejam substituídas ou excluídas."},"note_on_right":{"name":"Mostrar ícone à direita","note":"Mostrar o ícone da mensagem salva à direita."},"reduced_toasts":{"name":"Reduzir Notificações","note":"Reduz o número de notificações do sistema."}},"shortcuts":{"name":"Enviar atalhos para mensagens grandes","open_modal_shortcut":{"name":"Enviar Mensagem Atalho","note":"Atalho para abrir a caixa de diálogo Enviar mensagem grande."},"lock_message_shortcut":{"name":"Atalho de mensagem de bloqueio","note":"Atalho para bloquear a mensagem salva atualmente. Bloquear uma mensagem salva significa que ela não pode ser substituída ou excluída e não será restaurada na caixa de bate-papo. Ctrl + Clicar no ícone da mensagem salva também alternará o bloqueio."},"clear_message_shortcut":{"name":"Limpar atalho de mensagem","note":"Atalho para limpar a caixa de diálogo Enviar mensagem grande ou excluir a mensagem salva existente. Pressione novamente para restaurar a mensagem."}}}},"pt-BR":{"description":"Better Send Large Messages versão 2. Abre uma janela pop-up quando sua mensagem é muito grande, o que permite enviar automaticamente a mensagem como várias mensagens menores. A divisão de mensagens é evitada no meio de parágrafos e frases.","toast_allsent_text":"Todas as mensagens enviadas!","toast_restored_message_info":"Mensagem original restaurada.","toast_message_locked_info":"Mensagem salva bloqueada!","toast_message_unlocked_info":"Mensagem salva desbloqueada!","toast_locked_message_warning":"Mensagem salva bloqueada! Não é possível abrir o diálogo Enviar mensagem grande!","toast_deleted_message_warning":"Mensagem salva excluída!","toast_cleared_message_warning":"Mensagem apagada!","toast_undeleted_message_info":"Mensagem restaurada!","toast_cannot_delete_warning":"Não é possível excluir a mensagem bloqueada!","modal_messages_translation":"Mensagens","modal_messages_warning":"Não envie muitas mensagens!","modal_load_warning":"Mensagem salva correspondente carregada!","modal_overwrite_warning":"Última mensagem substituída!","modal_header_text":"Enviar mensagem grande","btn_cancel_text":"Cancelar","btn_send_text":"Mandar","note_tooltip":"Mensagem aberta","note_tooltip_locked":"Mensagem aberta [bloqueada]","settings":{"main_settings":{"name":"Configurações de envio de mensagens grandes","restore_message":{"name":"Restaurar mensagem","note":"Se você deve ou não restaurar sua mensagem na caixa de bate-papo ao fechar a caixa de diálogo Enviar mensagem grande, aparada pelo limite de mensagens.","options":{"always":"Sempre","never":"Nunca","short":"Para mensagens curtas"}},"lock_by_default":{"name":"Bloquear mensagens por padrão","note":"Bloqueie as mensagens salvas por padrão para evitar que sejam substituídas ou excluídas."},"note_on_right":{"name":"Mostrar ícone à direita","note":"Mostrar o ícone da mensagem salva à direita."},"reduced_toasts":{"name":"Reduzir Notificações","note":"Reduz o número de notificações do sistema."}},"shortcuts":{"name":"Enviar atalhos para mensagens grandes","open_modal_shortcut":{"name":"Enviar Mensagem Atalho","note":"Atalho para abrir a caixa de diálogo Enviar mensagem grande."},"lock_message_shortcut":{"name":"Atalho de mensagem de bloqueio","note":"Atalho para bloquear a mensagem salva atualmente. Bloquear uma mensagem salva significa que ela não pode ser substituída ou excluída e não será restaurada na caixa de bate-papo. Ctrl + Clicar no ícone da mensagem salva também alternará o bloqueio."},"clear_message_shortcut":{"name":"Limpar atalho de mensagem","note":"Atalho para limpar a caixa de diálogo Enviar mensagem grande ou excluir a mensagem salva existente. Pressione novamente para restaurar a mensagem."}}}},"ru":{"description":"Better Send Large Messages version 2. Открывает всплывающее окно, когда ваше сообщение слишком велико, что позволяет автоматически отправлять сообщение в виде нескольких небольших сообщений. Разделение сообщений избегается в середине абзацев и предложений.","toast_allsent_text":"Все сообщения отправлены!","toast_restored_message_info":"Исходное сообщение восстановлено.","toast_message_locked_info":"Сохраненное сообщение заблокировано!","toast_message_unlocked_info":"Сохраненное сообщение разблокировано!","toast_locked_message_warning":"Сохраненное сообщение заблокировано! Не могу открыть диалог Отправить большое сообщение!","toast_deleted_message_warning":"Сохраненное сообщение удалено!","toast_cleared_message_warning":"Сообщение очищено!","toast_undeleted_message_info":"Сообщение восстановлено!","toast_cannot_delete_warning":"Невозможно удалить заблокированное сообщение!","modal_messages_translation":"Сообщения","modal_messages_warning":"Не отправляйте слишком много сообщений!","modal_load_warning":"Загружено соответствующее сохраненное сообщение!","modal_overwrite_warning":"Последнее сообщение перезаписано!","modal_header_text":"Отправить большое сообщение","btn_cancel_text":"отменить","btn_send_text":"послать","note_tooltip":"Открытое сообщение","note_tooltip_locked":"Открытое сообщение [заблокировано]","settings":{"main_settings":{"name":"Настройки отправки больших сообщений","restore_message":{"name":"Восстановить сообщение","note":"Следует ли восстанавливать ваше сообщение в окне чата при закрытии диалогового окна «Отправить большое сообщение», обрезанного по пределу количества сообщений.","options":{"always":"Всегда","never":"Никогда","short":"Для коротких сообщений"}},"lock_by_default":{"name":"Блокировка сообщений по умолчанию","note":"Заблокируйте сохраненные сообщения по умолчанию, чтобы предотвратить их перезапись или удаление."},"note_on_right":{"name":"Показать значок справа","note":"Показать значок сохраненного сообщения справа."},"reduced_toasts":{"name":"Уменьшить уведомления","note":"Уменьшает количество уведомлений о тостах."}},"shortcuts":{"name":"Отправить большое сообщение ярлыки","open_modal_shortcut":{"name":"Отправить сообщение ярлык","note":"Ярлык, чтобы открыть диалоговое окно «Отправить большое сообщение»."},"lock_message_shortcut":{"name":"Блокировка сообщения","note":"Ярлык для блокировки текущего сохраненного сообщения. Блокировка сохраненного сообщения означает, что его нельзя перезаписать или удалить, и он не будет восстановлен в окне чата. Ctrl + Щелчок по значку сохраненного сообщения также переключит блокировку."},"clear_message_shortcut":{"name":"Очистить ярлык сообщения","note":"Ярлык, чтобы очистить диалоговое окно «Отправить большое сообщение» или удалить существующее сохраненное сообщение. Нажмите еще раз, чтобы восстановить сообщение."}}}},"sk":{"description":"Lepšia možnosť odosielania veľkých správ verzia 2. Otvorí sa kontextové okno, keď je vaša správa príliš veľká, čo vám umožňuje automaticky odoslať správu ako niekoľko menších správ. Rozdeleniu správ sa zabráni uprostred odsekov a viet.","toast_allsent_text":"Všetky správy boli odoslané!","toast_restored_message_info":"Pôvodná správa bola obnovená.","toast_message_locked_info":"Uložená správa je uzamknutá!","toast_message_unlocked_info":"Uložená správa bola odomknutá!","toast_locked_message_warning":"Uložená správa je uzamknutá! Nemožno otvoriť dialógové okno Odoslať veľkú správu!","toast_deleted_message_warning":"Uložená správa bola odstránená!","toast_cleared_message_warning":"Správa bola vymazaná!","toast_undeleted_message_info":"Správa bola obnovená!","toast_cannot_delete_warning":"Uzamknutú správu nie je možné odstrániť!","modal_messages_translation":"správy","modal_messages_warning":"Nezasielajte príliš veľa správ!","modal_load_warning":"Načítané zodpovedajúce uložené správy!","modal_overwrite_warning":"Posledná správa bola prepísaná!","modal_header_text":"Poslať veľkú správu","btn_cancel_text":"Zrušiť","btn_send_text":"odoslať","note_tooltip":"Otvoriť správu","note_tooltip_locked":"Otvoriť správu [zamknuté]","settings":{"main_settings":{"name":"Poslať nastavenia veľkých správ","restore_message":{"name":"Obnoviť správu","note":"Či sa má alebo nemá obnoviť vaša správa v chatboxe, keď zavriete dialógové okno Send Large Message, orezané limitom správy.","options":{"always":"vždy","never":"nikdy","short":"Pre krátke správy"}},"lock_by_default":{"name":"Predvolene uzamknúť správy","note":"Predvolene uzamknite uložené správy, aby ste zabránili ich prepísaniu alebo odstráneniu."},"note_on_right":{"name":"Zobraziť ikonu vpravo","note":"Zobraziť ikonu uloženej správy vpravo."},"reduced_toasts":{"name":"Znížte počet oznámení","note":"Znižuje počet oznámení o prípitkoch."}},"shortcuts":{"name":"Odoslať veľké odkazové správy","open_modal_shortcut":{"name":"Skratka na odoslanie správy","note":"Skratka na otvorenie dialógového okna Odoslať veľkú správu."},"lock_message_shortcut":{"name":"Skratka odkazovej správy","note":"Skratka na uzamknutie aktuálne uloženej správy. Uzamknutie uloženej správy znamená, že ju nemožno prepísať alebo odstrániť, a neobnoví sa do chatovacej schránky. Ctrl + Kliknutím na ikonu uloženej správy prepnete uzamknutie."},"clear_message_shortcut":{"name":"Vymazať odkaz na správu","note":"Skratka na vymazanie dialógového okna Odoslať veľkú správu alebo na odstránenie existujúcej uloženej správy. Opätovným stlačením správu obnovíte."}}}},"es":{"description":"Better Send Large Messages versión 2. Abre una ventana emergente cuando su mensaje es demasiado grande, lo que le permite enviar automáticamente el mensaje como varios mensajes más pequeños. La división de mensajes se evita en medio de párrafos y oraciones.","toast_allsent_text":"Todos los mensajes enviados!","toast_restored_message_info":"Mensaje original restaurado.","toast_message_locked_info":"Mensaje guardado bloqueado!","toast_message_unlocked_info":"Mensaje guardado desbloqueado!","toast_locked_message_warning":"Mensaje guardado bloqueado! ¡No se puede abrir el diálogo Enviar mensaje grande!","toast_deleted_message_warning":"Mensaje guardado eliminado!","toast_cleared_message_warning":"Mensaje borrado!","toast_undeleted_message_info":"Mensaje restaurado!","toast_cannot_delete_warning":"¡No se puede eliminar el mensaje bloqueado!","modal_messages_translation":"Mensajes","modal_messages_warning":"¡No envíes demasiados mensajes!","modal_load_warning":"¡Mensaje guardado coincidente cargado!","modal_overwrite_warning":"Último mensaje sobrescrito!","modal_header_text":"Enviar mensaje grande","btn_cancel_text":"Cancelar","btn_send_text":"Enviar","note_tooltip":"Mensaje abierto","note_tooltip_locked":"Mensaje abierto [bloqueado]","settings":{"main_settings":{"name":"Enviar configuración de mensajes grandes","restore_message":{"name":"Restaurar mensaje","note":"Si desea o no restaurar su mensaje en el chatbox cuando cierra el diálogo Enviar mensaje grande, recortado por el límite del mensaje.","options":{"always":"Siempre","never":"Nunca","short":"Para mensajes cortos"}},"lock_by_default":{"name":"Bloquear mensajes por defecto","note":"Bloquee los mensajes guardados de forma predeterminada para evitar que se sobrescriban o eliminen."},"note_on_right":{"name":"Mostrar icono a la derecha","note":"Mostrar el icono de mensaje guardado a la derecha."},"reduced_toasts":{"name":"Reducir notificaciones","note":"Reduce el número de notificaciones de tostadas."}},"shortcuts":{"name":"Enviar accesos directos de mensajes grandes","open_modal_shortcut":{"name":"Enviar acceso directo a mensaje","note":"Atajo para abrir el diálogo Enviar mensaje grande."},"lock_message_shortcut":{"name":"Bloquear acceso directo de mensaje","note":"Atajo para bloquear el mensaje guardado actual. Bloquear un mensaje guardado significa que no se puede sobrescribir o eliminar y no se restaurará en el chatbox. Ctrl + Hacer clic en el icono del mensaje guardado también alternará el bloqueo."},"clear_message_shortcut":{"name":"Borrar atajo de mensaje","note":"Atajo para borrar el diálogo Enviar mensaje grande o eliminar el mensaje guardado existente. Presione nuevamente para restaurar el mensaje."}}}},"es-ES":{"description":"Better Send Large Messages versión 2. Abre una ventana emergente cuando su mensaje es demasiado grande, lo que le permite enviar automáticamente el mensaje como varios mensajes más pequeños. La división de mensajes se evita en medio de párrafos y oraciones.","toast_allsent_text":"Todos los mensajes enviados!","toast_restored_message_info":"Mensaje original restaurado.","toast_message_locked_info":"Mensaje guardado bloqueado!","toast_message_unlocked_info":"Mensaje guardado desbloqueado!","toast_locked_message_warning":"Mensaje guardado bloqueado! ¡No se puede abrir el diálogo Enviar mensaje grande!","toast_deleted_message_warning":"Mensaje guardado eliminado!","toast_cleared_message_warning":"Mensaje borrado!","toast_undeleted_message_info":"Mensaje restaurado!","toast_cannot_delete_warning":"¡No se puede eliminar el mensaje bloqueado!","modal_messages_translation":"Mensajes","modal_messages_warning":"¡No envíes demasiados mensajes!","modal_load_warning":"¡Mensaje guardado coincidente cargado!","modal_overwrite_warning":"Último mensaje sobrescrito!","modal_header_text":"Enviar mensaje grande","btn_cancel_text":"Cancelar","btn_send_text":"Enviar","note_tooltip":"Mensaje abierto","note_tooltip_locked":"Mensaje abierto [bloqueado]","settings":{"main_settings":{"name":"Enviar configuración de mensajes grandes","restore_message":{"name":"Restaurar mensaje","note":"Si desea o no restaurar su mensaje en el chatbox cuando cierra el diálogo Enviar mensaje grande, recortado por el límite del mensaje.","options":{"always":"Siempre","never":"Nunca","short":"Para mensajes cortos"}},"lock_by_default":{"name":"Bloquear mensajes por defecto","note":"Bloquee los mensajes guardados de forma predeterminada para evitar que se sobrescriban o eliminen."},"note_on_right":{"name":"Mostrar icono a la derecha","note":"Mostrar el icono de mensaje guardado a la derecha."},"reduced_toasts":{"name":"Reducir notificaciones","note":"Reduce el número de notificaciones de tostadas."}},"shortcuts":{"name":"Enviar accesos directos de mensajes grandes","open_modal_shortcut":{"name":"Enviar acceso directo a mensaje","note":"Atajo para abrir el diálogo Enviar mensaje grande."},"lock_message_shortcut":{"name":"Bloquear acceso directo de mensaje","note":"Atajo para bloquear el mensaje guardado actual. Bloquear un mensaje guardado significa que no se puede sobrescribir o eliminar y no se restaurará en el chatbox. Ctrl + Hacer clic en el icono del mensaje guardado también alternará el bloqueo."},"clear_message_shortcut":{"name":"Borrar atajo de mensaje","note":"Atajo para borrar el diálogo Enviar mensaje grande o eliminar el mensaje guardado existente. Presione nuevamente para restaurar el mensaje."}}}},"sv":{"description":"Bättre skicka stora meddelanden version 2. Öppnar en popup när ditt meddelande är för stort, vilket gör att du automatiskt kan skicka meddelandet som flera mindre meddelanden. Dela meddelanden undviks i mitten av stycken och meningarna.","toast_allsent_text":"Alla meddelanden skickas!","toast_restored_message_info":"Originalmeddelande återställd.","toast_message_locked_info":"Sparat meddelande låst!","toast_message_unlocked_info":"Sparat meddelande låst upp!","toast_locked_message_warning":"Sparat meddelande låst! Kan inte öppna Skicka stort meddelande dialog!","toast_deleted_message_warning":"Sparat meddelande raderat!","toast_cleared_message_warning":"Meddelandet rensas!","toast_undeleted_message_info":"Meddelandet återställs!","toast_cannot_delete_warning":"Det går inte att ta bort låst meddelande!","modal_messages_translation":"meddelanden","modal_messages_warning":"Skicka inte för många meddelanden!","modal_load_warning":"Laddad matchande sparat meddelande!","modal_overwrite_warning":"Det sista meddelandet skrivs över!","modal_header_text":"Skicka stort meddelande","btn_cancel_text":"Annullera","btn_send_text":"Skicka","note_tooltip":"Öppna meddelandet","note_tooltip_locked":"Öppna meddelandet [låst]","settings":{"main_settings":{"name":"Skicka inställningar för stora meddelanden","restore_message":{"name":"Återställ meddelandet","note":"Huruvida du vill återställa ditt meddelande till chatboxen när du stänger dialogrutan Skicka stort meddelande, trimmad efter meddelandegräns.","options":{"always":"Alltid","never":"Aldrig","short":"För korta meddelanden"}},"lock_by_default":{"name":"Lås meddelanden som standard","note":"Lås sparade meddelanden som standard för att förhindra att de skrivs över eller raderas."},"note_on_right":{"name":"Visa ikon till höger","note":"Visa sparat meddelandeikon till höger."},"reduced_toasts":{"name":"Minska aviseringar","note":"Minskar antalet toastmeddelanden."}},"shortcuts":{"name":"Skicka stora genvägar för meddelanden","open_modal_shortcut":{"name":"Skicka meddelandegenväg","note":"Genväg för att öppna dialogrutan Skicka stort meddelande."},"lock_message_shortcut":{"name":"Lås genväg för meddelande","note":"Genväg för att låsa det nuvarande sparade meddelandet. Att låsa ett sparat meddelande innebär att det inte kan skrivas över eller raderas och kommer inte att återställas till chatboxen. Ctrl + Om du klickar på den sparade meddelandesymbolen växlar också låsning."},"clear_message_shortcut":{"name":"Rensa genväg för meddelande","note":"Genväg för att rensa dialogen Skicka stort meddelande eller ta bort det befintliga sparade meddelandet. Tryck igen för att återställa meddelandet."}}}},"sv-SE":{"description":"Bättre skicka stora meddelanden version 2. Öppnar en popup när ditt meddelande är för stort, vilket gör att du automatiskt kan skicka meddelandet som flera mindre meddelanden. Dela meddelanden undviks i mitten av stycken och meningarna.","toast_allsent_text":"Alla meddelanden skickas!","toast_restored_message_info":"Originalmeddelande återställd.","toast_message_locked_info":"Sparat meddelande låst!","toast_message_unlocked_info":"Sparat meddelande låst upp!","toast_locked_message_warning":"Sparat meddelande låst! Kan inte öppna Skicka stort meddelande dialog!","toast_deleted_message_warning":"Sparat meddelande raderat!","toast_cleared_message_warning":"Meddelandet rensas!","toast_undeleted_message_info":"Meddelandet återställs!","toast_cannot_delete_warning":"Det går inte att ta bort låst meddelande!","modal_messages_translation":"meddelanden","modal_messages_warning":"Skicka inte för många meddelanden!","modal_load_warning":"Laddad matchande sparat meddelande!","modal_overwrite_warning":"Det sista meddelandet skrivs över!","modal_header_text":"Skicka stort meddelande","btn_cancel_text":"Annullera","btn_send_text":"Skicka","note_tooltip":"Öppna meddelandet","note_tooltip_locked":"Öppna meddelandet [låst]","settings":{"main_settings":{"name":"Skicka inställningar för stora meddelanden","restore_message":{"name":"Återställ meddelandet","note":"Huruvida du vill återställa ditt meddelande till chatboxen när du stänger dialogrutan Skicka stort meddelande, trimmad efter meddelandegräns.","options":{"always":"Alltid","never":"Aldrig","short":"För korta meddelanden"}},"lock_by_default":{"name":"Lås meddelanden som standard","note":"Lås sparade meddelanden som standard för att förhindra att de skrivs över eller raderas."},"note_on_right":{"name":"Visa ikon till höger","note":"Visa sparat meddelandeikon till höger."},"reduced_toasts":{"name":"Minska aviseringar","note":"Minskar antalet toastmeddelanden."}},"shortcuts":{"name":"Skicka stora genvägar för meddelanden","open_modal_shortcut":{"name":"Skicka meddelandegenväg","note":"Genväg för att öppna dialogrutan Skicka stort meddelande."},"lock_message_shortcut":{"name":"Lås genväg för meddelande","note":"Genväg för att låsa det nuvarande sparade meddelandet. Att låsa ett sparat meddelande innebär att det inte kan skrivas över eller raderas och kommer inte att återställas till chatboxen. Ctrl + Om du klickar på den sparade meddelandesymbolen växlar också låsning."},"clear_message_shortcut":{"name":"Rensa genväg för meddelande","note":"Genväg för att rensa dialogen Skicka stort meddelande eller ta bort det befintliga sparade meddelandet. Tryck igen för att återställa meddelandet."}}}},"tr":{"description":"Daha İyi Büyük Mesajlar Gönder sürümü 2. Mesajınız çok büyük olduğunda, mesaj otomatik olarak daha küçük mesajlar halinde göndermenize olanak sağlayan bir pop-up açar. Paragraf ve cümlelerin ortasında mesaj bölme işleminden kaçınılır.","toast_allsent_text":"Tüm mesajlar gönderildi!","toast_restored_message_info":"Orijinal mesaj geri yüklendi.","toast_message_locked_info":"Kayıtlı mesaj kilitli!","toast_message_unlocked_info":"Kayıtlı mesajın kilidi açıldı!","toast_locked_message_warning":"Kayıtlı mesaj kilitli! Büyük Mesaj Gönder diyalogunu açamıyorum!","toast_deleted_message_warning":"Kayıtlı mesaj silindi!","toast_cleared_message_warning":"Mesaj temizlendi!","toast_undeleted_message_info":"Mesaj geri yüklendi!","toast_cannot_delete_warning":"Kilitli mesajı silemiyorum!","modal_messages_translation":"Mesajlar","modal_messages_warning":"Çok fazla mesaj göndermeyin!","modal_load_warning":"Yüklenen eşleştirme kaydedilmiş mesaj!","modal_overwrite_warning":"Son mesajın üzerine yazıldı!","modal_header_text":"Büyük Mesaj Gönder","btn_cancel_text":"İptal etmek","btn_send_text":"göndermek","note_tooltip":"Mesajı aç","note_tooltip_locked":"Mesajı aç [kilitli]","settings":{"main_settings":{"name":"Büyük Mesaj Ayarlarını Gönder","restore_message":{"name":"Mesajı Geri Yükle","note":"Mesaj Sınırı ile Kesilmiş Büyük Mesaj Gönder diyalog penceresini kapattığınızda mesajınızın sohbet kutusuna geri yüklenip yüklenmeyeceği.","options":{"always":"Her zaman","never":"Asla","short":"Kısa mesajlar için"}},"lock_by_default":{"name":"Mesajları Varsayılan Olarak Kilitle","note":"Üzerine yazmalarını veya silinmelerini önlemek için kayıtlı mesajları varsayılan olarak kilitleyin."},"note_on_right":{"name":"Simgeyi Sağda Göster","note":"Sağdaki kayıtlı mesaj simgesini göster."},"reduced_toasts":{"name":"Bildirimleri Azalt","note":"Kızarmış ekmek bildirimi sayısını azaltır."}},"shortcuts":{"name":"Büyük Mesaj Kısayolları Gönder","open_modal_shortcut":{"name":"Mesaj Kısayolu Gönder","note":"Büyük Mesaj Gönder diyalog penceresini açmak için kısayol."},"lock_message_shortcut":{"name":"İleti Kısayolunu Kilitle","note":"Kayıtlı mevcut mesajı kilitlemek için kısayol. Kayıtlı bir mesajı kilitlemek, üzerine yazılamadığı veya silinemediği ve sohbet kutusuna geri yüklenmeyeceği anlamına gelir. Ctrl + Kaydedilen mesaj simgesine tıklamak da kilitlenmeyi değiştirir."},"clear_message_shortcut":{"name":"İleti Kısayolunu Temizle","note":"Büyük Mesaj Gönder diyalog penceresini silmek veya mevcut kayıtlı mesajı silmek için kısayol. Mesajı geri yüklemek için tekrar basın."}}}},"bg":{"description":"По-добре изпратете големи съобщения версия 2. Отваря изскачащ прозорец, когато съобщението ви е твърде голямо, което ви позволява автоматично да изпращате съобщението като няколко по-малки съобщения. Избягване на разделянето на съобщенията в средата на абзаците и изреченията.","toast_allsent_text":"Всички изпратени съобщения!","toast_restored_message_info":"Оригиналното съобщение е възстановено.","toast_message_locked_info":"Запазеното съобщение е заключено!","toast_message_unlocked_info":"Запазеното съобщение е отключено!","toast_locked_message_warning":"Запазеното съобщение е заключено! Не може да се отвори диалог за изпращане на голямо съобщение!","toast_deleted_message_warning":"Запазеното съобщение е изтрито!","toast_cleared_message_warning":"Съобщението е изчистено!","toast_undeleted_message_info":"Съобщението е възстановено!","toast_cannot_delete_warning":"Не може да се изтрие заключеното съобщение!","modal_messages_translation":"Съобщения","modal_messages_warning":"Не изпращайте твърде много съобщения!","modal_load_warning":"Заредено съвпадение на запазено съобщение!","modal_overwrite_warning":"Последно съобщение е презаписано!","modal_header_text":"Изпратете голямо съобщение","btn_cancel_text":"Отказ","btn_send_text":"изпращам","note_tooltip":"Отворено съобщение","note_tooltip_locked":"Отворено съобщение [заключено]","settings":{"main_settings":{"name":"Изпращане на големи съобщения","restore_message":{"name":"Възстановяване на съобщението","note":"Дали да възстановите съобщението си в чатбокса, когато затворите диалога Изпращане на голямо съобщение, подрязан от ограничението за съобщения.","options":{"always":"Винаги","never":"никога","short":"За кратки съобщения"}},"lock_by_default":{"name":"Заключване на съобщенията по подразбиране","note":"Заключете запаметените съобщения по подразбиране, за да предотвратите тяхното презаписване или изтриване."},"note_on_right":{"name":"Показване на икона отдясно","note":"Показване на икона за запазено съобщение вдясно."},"reduced_toasts":{"name":"Намалете известията","note":"Намалява броя известия за тост."}},"shortcuts":{"name":"Изпращане на големи преки съобщения","open_modal_shortcut":{"name":"Изпращане на съобщение пряк път","note":"Пряк път за отваряне на диалога за изпращане на голямо съобщение."},"lock_message_shortcut":{"name":"Пряк път за заключване на съобщението","note":"Пряк път за заключване на текущо запазеното съобщение. Заключването на запазено съобщение означава, че то не може да бъде презаписано или изтрито и няма да се възстанови в чатбокса. Ctrl + Щракването върху иконата на запазено съобщение също ще превключва заключването."},"clear_message_shortcut":{"name":"Пряк път за ясно съобщение","note":"Пряк път за изчистване на диалоговия прозорец Изпращане на голямо съобщение или изтриване на съществуващото запазено съобщение. Натиснете отново, за да възстановите съобщението."}}}},"uk":{"description":"Краще надіслати великі повідомлення версії 2. Відкриває спливаюче вікно, коли ваше повідомлення занадто велике, що дозволяє автоматично надсилати повідомлення як кілька менших повідомлень. Уникнути розбиття повідомлень у середині абзаців та речень.","toast_allsent_text":"Усі надіслані повідомлення!","toast_restored_message_info":"Оригінальне повідомлення відновлено.","toast_message_locked_info":"Збережене повідомлення заблоковано!","toast_message_unlocked_info":"Збережене повідомлення розблоковано!","toast_locked_message_warning":"Збережене повідомлення заблоковано! Не вдається відкрити діалог великого повідомлення!","toast_deleted_message_warning":"Збережене повідомлення видалено!","toast_cleared_message_warning":"Повідомлення очищено!","toast_undeleted_message_info":"Повідомлення відновлено!","toast_cannot_delete_warning":"Неможливо видалити заблоковане повідомлення!","modal_messages_translation":"Повідомлення","modal_messages_warning":"Не надсилайте занадто багато повідомлень!","modal_load_warning":"Завантажено відповідне збережене повідомлення!","modal_overwrite_warning":"Останнє повідомлення перезаписано!","modal_header_text":"Надіслати велике повідомлення","btn_cancel_text":"Скасувати","btn_send_text":"Надіслати","note_tooltip":"Відкрити повідомлення","note_tooltip_locked":"Відкрити повідомлення [заблоковано]","settings":{"main_settings":{"name":"Надіслати налаштування великих повідомлень","restore_message":{"name":"Відновити повідомлення","note":"Потрібно відновити ваше повідомлення в чаті, коли ви закриєте діалог \"Надіслати велике повідомлення\", оброблений лімітом повідомлень.","options":{"always":"Завжди","never":"Ніколи","short":"Для коротких повідомлень"}},"lock_by_default":{"name":"Блокування повідомлень за замовчуванням","note":"Заблокуйте збережені повідомлення за замовчуванням, щоб запобігти їх перезапису чи видаленню."},"note_on_right":{"name":"Показати значок справа","note":"Показати значок збереженого повідомлення праворуч."},"reduced_toasts":{"name":"Зменшити сповіщення","note":"Зменшує кількість сповіщень про тости."}},"shortcuts":{"name":"Надіслати великі ярлики повідомлень","open_modal_shortcut":{"name":"Надіслати ярлик","note":"Ярлик для відкриття діалогу \"Надіслати велике повідомлення\"."},"lock_message_shortcut":{"name":"Ярлик повідомлення про блокування","note":"Ярлик для блокування поточного збереженого повідомлення. Блокування збереженого повідомлення означає, що його неможливо перезаписати чи видалити і не відновиться у чаті. Ctrl + Клацання значка збереженого повідомлення також переключить блокування."},"clear_message_shortcut":{"name":"Ясний ярлик повідомлення","note":"Ярлик для очищення діалогового вікна \"Надіслати велике повідомлення\" або видалення наявного збереженого повідомлення. Натисніть знову, щоб відновити повідомлення."}}}},"fi":{"description":"Parempien lähetysten isojen viestien versio 2. Avaa ponnahdusikkunan, kun viestisi on liian suuri, jolloin voit lähettää viestin automaattisesti useana pienenä viestinä. Viestien jakamista vältetään kappaleiden ja lauseiden keskellä.","toast_allsent_text":"Kaikki viestit lähetetty!","toast_restored_message_info":"Alkuperäinen viesti palautettu.","toast_message_locked_info":"Tallennettu viesti lukittu!","toast_message_unlocked_info":"Tallennettu viesti avattu!","toast_locked_message_warning":"Tallennettu viesti lukittu! Lähetä iso viesti -dialogia ei voi avata!","toast_deleted_message_warning":"Tallennettu viesti poistettu!","toast_cleared_message_warning":"Viesti poistettu!","toast_undeleted_message_info":"Viesti palautettu!","toast_cannot_delete_warning":"Lukittua viestiä ei voi poistaa!","modal_messages_translation":"viestien","modal_messages_warning":"Älä lähetä liian monta viestiä!","modal_load_warning":"Ladattu vastaava tallennettu viesti!","modal_overwrite_warning":"Viimeinen viesti korvataan!","modal_header_text":"Lähetä iso viesti","btn_cancel_text":"Peruuttaa","btn_send_text":"Lähettää","note_tooltip":"Avaa viesti","note_tooltip_locked":"Avaa viesti [lukittu]","settings":{"main_settings":{"name":"Lähetä isojen viestien asetukset","restore_message":{"name":"Palauta viesti","note":"Palautetaanko viesti takaisin chat-ruutuun sulkeessasi Lähetä iso viesti -valintaikkuna, rajattuna viestirajassa.","options":{"always":"Aina","never":"Ei koskaan","short":"Lyhytsanomat"}},"lock_by_default":{"name":"Lukitse viestit oletuksena","note":"Lukitse tallennetut viestit oletuksena, jotta niitä ei korvata tai poistaa."},"note_on_right":{"name":"Näytä kuvake oikealla","note":"Näytä tallennetun viestin kuvake oikealla."},"reduced_toasts":{"name":"Vähennä ilmoituksia","note":"Vähentää paahtoleipäilmoitusten määrää."}},"shortcuts":{"name":"Lähetä iso viestin pikakuvakkeet","open_modal_shortcut":{"name":"Lähetä viesti pikakuvake","note":"Pikakuvake Avaa iso viesti -valintaikkuna."},"lock_message_shortcut":{"name":"Lukitse viestin pikakuvake","note":"Pikakuvake lukitsemaan nykyinen tallennettu viesti. Tallennetun viestin lukitseminen tarkoittaa, että sitä ei voida korvata tai poistaa, eikä sitä palauteta chatboxiin. Ctrl + Tallennetun viestin kuvakkeen napsauttaminen myös vaihtaa lukituksen."},"clear_message_shortcut":{"name":"Tyhjennä viestin pikakuvake","note":"Pikakuvake tyhjentää Lähetä iso viesti -valintaikkuna tai poista olemassa oleva tallennettu viesti. Palauta viesti painamalla uudelleen."}}}},"no":{"description":"Better Send Large Messages version 2. Åpner en pop-up når meldingen er for stor, som lar deg automatisk sende meldingen som flere mindre meldinger. Meldingssplitting unngås midt i avsnitt og setninger.","toast_allsent_text":"Alle meldinger sendt!","toast_restored_message_info":"Opprinnelig melding gjenopprettet.","toast_message_locked_info":"Lagret melding låst!","toast_message_unlocked_info":"Lagret melding låst opp!","toast_locked_message_warning":"Lagret melding låst! Kan ikke åpne Send Large Message-dialog!","toast_deleted_message_warning":"Lagret melding er slettet!","toast_cleared_message_warning":"Melding ryddet!","toast_undeleted_message_info":"Melding gjenopprettet!","toast_cannot_delete_warning":"Kan ikke slette låst melding!","modal_messages_translation":"meldinger","modal_messages_warning":"Ikke send for mange meldinger!","modal_load_warning":"Lastet samsvarende lagret melding!","modal_overwrite_warning":"Siste melding overskrevet!","modal_header_text":"Send stor melding","btn_cancel_text":"Avbryt","btn_send_text":"Sende","note_tooltip":"Åpen melding","note_tooltip_locked":"Åpne melding [låst]","settings":{"main_settings":{"name":"Send innstillinger for store meldinger","restore_message":{"name":"Gjenopprett melding","note":"Hvorvidt du vil gjenopprette meldingen din i chatboksen når du lukker dialogboksen Send stor melding, trimmet etter meldingsgrense.","options":{"always":"Alltid","never":"Aldri","short":"For korte meldinger"}},"lock_by_default":{"name":"Lås meldinger som standard","note":"Lås lagrede meldinger som standard for å forhindre at de blir overskrevet eller slettet."},"note_on_right":{"name":"Vis ikon til høyre","note":"Vis lagret meldingsikon til høyre."},"reduced_toasts":{"name":"Reduser varslinger","note":"Reduserer antall toastvarsler."}},"shortcuts":{"name":"Send snarveier til store meldinger","open_modal_shortcut":{"name":"Send meldingssnarvei","note":"Snarvei for å åpne dialogboksen Send stor melding."},"lock_message_shortcut":{"name":"Lås snarvei for melding","note":"Snarvei for å låse den gjeldende lagrede meldingen. Å låse en lagret melding betyr at den ikke kan overskrives eller slettes og vil ikke gjenopprette til chatboksen. Ctrl + Når du klikker på det lagrede melding-ikonet, veksler du også låsing."},"clear_message_shortcut":{"name":"Tøm snarvei for melding","note":"Snarvei for å fjerne dialogboksen Send stor melding eller slette den eksisterende lagrede meldingen. Trykk igjen for å gjenopprette meldingen."}}}},"hr":{"description":"Bolje slanje velikih poruka verzija 2. Otvara se popout kada je vaša poruka prevelika, što vam omogućuje automatsko slanje poruke kao nekoliko manjih poruka. Izbjegava se dijeljenje poruka u sredini odlomaka i rečenica.","toast_allsent_text":"Sve poruke poslane!","toast_restored_message_info":"Izvorna poruka vraćena.","toast_message_locked_info":"Spremljena poruka zaključana!","toast_message_unlocked_info":"Spremljena je poruka otključana!","toast_locked_message_warning":"Spremljena poruka zaključana! Ne mogu otvoriti dijalog slanja velike poruke!","toast_deleted_message_warning":"Spremljena poruka je izbrisana!","toast_cleared_message_warning":"Poruka je očišćena!","toast_undeleted_message_info":"Poruka je vraćena!","toast_cannot_delete_warning":"Ne može se izbrisati zaključana poruka!","modal_messages_translation":"poruke","modal_messages_warning":"Ne šaljite previše poruka!","modal_load_warning":"Učitana je odgovarajuća spremljena poruka!","modal_overwrite_warning":"Zadnja poruka prepisana!","modal_header_text":"Pošalji veliku poruku","btn_cancel_text":"Otkazati","btn_send_text":"Poslati","note_tooltip":"Otvori poruku","note_tooltip_locked":"Otvori poruku [zaključano]","settings":{"main_settings":{"name":"Pošaljite postavke velikih poruka","restore_message":{"name":"Vrati poruku","note":"Bez obzira želite li vratiti poruku u chatbox ako ne zatvorite dijalog Send Large Message, obrezan ograničenjem poruke.","options":{"always":"Stalno","never":"Nikada","short":"Za kratke poruke"}},"lock_by_default":{"name":"Zaključaj poruke prema zadanim postavkama","note":"Spremite spremljene poruke prema zadanim postavkama kako biste spriječili prepisivanje ili brisanje."},"note_on_right":{"name":"Prikaži ikonu s desne strane","note":"Pokažite ikonu spremljene poruke s desne strane."},"reduced_toasts":{"name":"Smanjite obavijesti","note":"Smanjuje broj obavijesti o zdravici."}},"shortcuts":{"name":"Pošaljite velike prečice za poruke","open_modal_shortcut":{"name":"Pošalji poruku Prečac","note":"Prečac za otvaranje dijaloga Pošalji veliku poruku."},"lock_message_shortcut":{"name":"Prečac za zaključavanje poruke","note":"Prečac za zaključavanje trenutno spremljene poruke. Zaključavanje spremljene poruke znači da je ne možete prebrisati ili izbrisati i neće se vratiti u chatbox. Ctrl + Klikom na ikonu spremljene poruke također će se isključiti zaključavanje."},"clear_message_shortcut":{"name":"Prečista poruka poruke","note":"Prečac za brisanje dijaloga Send Large Message ili brisanje postojeće spremljene poruke. Ponovno pritisnite za vraćanje poruke."}}}},"ro":{"description":"Mai bine Trimiteți mesaje mari versiunea 2. Deschide o fereastră emergentă când mesajul dvs. este prea mare, ceea ce vă permite să trimiteți automat mesajul ca mai multe mesaje mai mici. Divizarea mesajelor este evitată în mijlocul alineatelor și propozițiilor.","toast_allsent_text":"Toate mesajele trimise!","toast_restored_message_info":"Mesajul original a fost restabilit.","toast_message_locked_info":"Mesajul salvat este blocat!","toast_message_unlocked_info":"Mesajul salvat a fost deblocat!","toast_locked_message_warning":"Mesajul salvat este blocat! Nu se poate deschide Trimite mesaj mare!","toast_deleted_message_warning":"Mesajul salvat a fost șters!","toast_cleared_message_warning":"Mesajul a fost șters!","toast_undeleted_message_info":"Mesaj restabilit!","toast_cannot_delete_warning":"Nu se poate șterge mesajul blocat!","modal_messages_translation":"Mesaje","modal_messages_warning":"Nu trimiteți prea multe mesaje!","modal_load_warning":"S-a încărcat mesajul salvat!","modal_overwrite_warning":"Ultimul mesaj suprascris!","modal_header_text":"Trimite mesaj mare","btn_cancel_text":"Anulare","btn_send_text":"Trimite","note_tooltip":"Mesaj deschis","note_tooltip_locked":"Deschide mesajul [blocat]","settings":{"main_settings":{"name":"Trimiteți mesaje mari setări","restore_message":{"name":"Restaurați mesajul","note":"Indiferent dacă vă restabiliți sau nu mesajul în caseta de chat atunci când închideți dialogul Trimite mesaj mare, decupat de limita mesajului.","options":{"always":"Mereu","never":"Nu","short":"Pentru mesaje scurte"}},"lock_by_default":{"name":"Blocați mesaje în mod implicit","note":"Blocați mesajele salvate în mod implicit pentru a le împiedica să fie suprascrise sau șterse."},"note_on_right":{"name":"Afișează pictograma din dreapta","note":"Afișează pictograma mesajului salvat în partea dreaptă."},"reduced_toasts":{"name":"Reduceți notificările","note":"Reduce numărul de notificări de toast."}},"shortcuts":{"name":"Trimiteți scurtături rapide pentru mesaje","open_modal_shortcut":{"name":"Trimitere rapidă de mesaj","note":"Comenză rapidă pentru a deschide dialogul Trimite mesaj mare."},"lock_message_shortcut":{"name":"Comutare scurtă de mesaje","note":"Comenză rapidă pentru a bloca mesajul salvat curent. Blocarea unui mesaj salvat înseamnă că nu poate fi suprascris sau șters și nu se va restabili la căsuța de chat. Ctrl + Făcând clic pe pictograma mesajului salvat va comuta și blocarea."},"clear_message_shortcut":{"name":"Ștergeți comanda rapidă de mesaje","note":"Comenză rapidă pentru a șterge dialogul Trimite mesaj mare sau pentru a șterge mesajul salvat existent. Apăsați din nou pentru a restabili mesajul."}}}},"lt":{"description":"Geriau siųsti didelius pranešimus, 2. versija. Atsiveria iššokantis langas, kai jūsų pranešimas yra per didelis, kuris leidžia jums automatiškai siųsti pranešimą kaip kelis mažesnius pranešimus. Vengiama pranešimų skaidymo pastraipų ir sakinių viduryje.","toast_allsent_text":"Visos žinutės išsiųstos!","toast_restored_message_info":"Originalus pranešimas atkurtas.","toast_message_locked_info":"Išsaugota žinutė užrakinta!","toast_message_unlocked_info":"Išsaugota žinutė atrakinta!","toast_locked_message_warning":"Išsaugota žinutė užrakinta! Neįmanoma atidaryti dialogo Siųsti didelę žinutę!","toast_deleted_message_warning":"Išsaugota žinutė ištrinta!","toast_cleared_message_warning":"Pranešimas išvalytas!","toast_undeleted_message_info":"Žinutė atkurta!","toast_cannot_delete_warning":"Neįmanoma ištrinti užrakinto pranešimo!","modal_messages_translation":"Žinutės","modal_messages_warning":"Nesiųskite per daug pranešimų!","modal_load_warning":"Įkelta atitikimo išsaugota žinutė!","modal_overwrite_warning":"Paskutinė žinutė perrašyta!","modal_header_text":"Siųsti didelę žinutę","btn_cancel_text":"Atšaukti","btn_send_text":"Siųsti","note_tooltip":"Atidaryti pranešimą","note_tooltip_locked":"Atidaryti pranešimą [užrakinta]","settings":{"main_settings":{"name":"Siųsti didelių pranešimų nustatymus","restore_message":{"name":"Atkurti pranešimą","note":"Ar norite atkurti savo pranešimą pokalbių dėžutėje, kai uždarote dialogo langą Siųsti didelį pranešimą, apribotą pranešimų skaičiumi.","options":{"always":"Visada","never":"Niekada","short":"Trumpoms žinutėms"}},"lock_by_default":{"name":"Užrakinti pranešimus pagal numatytuosius nustatymus","note":"Užrakinkite išsaugotus pranešimus pagal numatytuosius nustatymus, kad jie nebūtų perrašyti ar ištrinti."},"note_on_right":{"name":"Rodyti piktogramą dešinėje","note":"Rodyti išsaugotos žinutės piktogramą dešinėje."},"reduced_toasts":{"name":"Sumažinkite pranešimų skaičių","note":"Sumažina skrebučių pranešimų skaičių."}},"shortcuts":{"name":"Siųsti didelius pranešimų sparčiuosius klavišus","open_modal_shortcut":{"name":"Siųsti žinutės nuorodą","note":"Spartusis dialogo lango Siųsti didelį pranešimą atidarymas."},"lock_message_shortcut":{"name":"Užrakinti pranešimo nuorodą","note":"Nuoroda dabartiniam išsaugotam pranešimui užrakinti. Jei užrakinote išsaugotą pranešimą, jis negali būti perrašytas ar ištrintas ir nebus atkurtas pokalbių dėžutėje. „Ctrl“ + Spustelėję išsaugotos žinutės piktogramą, užraktas taip pat bus perjungiamas."},"clear_message_shortcut":{"name":"Išvalyti pranešimo nuorodą","note":"Nuoroda, jei norite ištrinti dialogo langą Siųsti didelį pranešimą arba ištrinti esamą išsaugotą pranešimą. Paspauskite dar kartą, jei norite atkurti pranešimą."}}}},"th":{"description":"ดีกว่าส่งข้อความขนาดใหญ่เวอร์ชั่น 2 เปิดป๊อปอัปเมื่อข้อความของคุณมีขนาดใหญ่เกินไปซึ่งช่วยให้คุณสามารถส่งข้อความโดยอัตโนมัติเป็นข้อความเล็ก ๆ หลายข้อความ การแยกข้อความจะหลีกเลี่ยงในช่วงกลางของย่อหน้าและประโยค","toast_allsent_text":"ส่งข้อความทั้งหมดแล้ว!","toast_restored_message_info":"กู้คืนข้อความต้นฉบับ","toast_message_locked_info":"ข้อความที่บันทึกไว้ถูกล็อค!","toast_message_unlocked_info":"ปลดล็อคข้อความที่บันทึกแล้ว!","toast_locked_message_warning":"ข้อความที่บันทึกไว้ถูกล็อค! ไม่สามารถเปิดกล่องโต้ตอบส่งข้อความขนาดใหญ่!","toast_deleted_message_warning":"ลบข้อความที่บันทึกแล้ว!","toast_cleared_message_warning":"ล้างข้อความแล้ว!","toast_undeleted_message_info":"กู้คืนข้อความแล้ว!","toast_cannot_delete_warning":"ไม่สามารถลบข้อความที่ล็อค!","modal_messages_translation":"ข้อความ","modal_messages_warning":"อย่าส่งข้อความมากเกินไป!","modal_load_warning":"จับคู่ข้อความที่โหลดแล้ว!","modal_overwrite_warning":"ข้อความล่าสุดถูกเขียนทับ!","modal_header_text":"ส่งข้อความขนาดใหญ่","btn_cancel_text":"ยกเลิก","btn_send_text":"ส่ง","note_tooltip":"เปิดข้อความ","note_tooltip_locked":"เปิดข้อความ [ล็อค]","settings":{"main_settings":{"name":"ส่งการตั้งค่าข้อความขนาดใหญ่","restore_message":{"name":"กู้คืนข้อความ","note":"ไม่ว่าจะกู้คืนข้อความของคุณไปยังกล่องสนทนาหรือไม่เมื่อคุณปิดกล่องโต้ตอบส่งข้อความขนาดใหญ่","options":{"always":"เสมอ","never":"ไม่เคย","short":"สำหรับข้อความสั้น ๆ"}},"lock_by_default":{"name":"ล็อคข้อความตามค่าเริ่มต้น","note":"ล็อคข้อความที่บันทึกไว้ตามค่าเริ่มต้นเพื่อป้องกันไม่ให้เขียนทับหรือลบทิ้ง"},"note_on_right":{"name":"แสดงไอคอนทางด้านขวา","note":"แสดงไอคอนข้อความที่บันทึกทางด้านขวา"},"reduced_toasts":{"name":"ลดการแจ้งเตือน","note":"ลดจำนวนการแจ้งเตือนของขนมปัง"}},"shortcuts":{"name":"ส่งทางลัดข้อความขนาดใหญ่","open_modal_shortcut":{"name":"ส่งข้อความทางลัด","note":"แป้นพิมพ์ลัดเพื่อเปิดกล่องโต้ตอบส่งข้อความขนาดใหญ่"},"lock_message_shortcut":{"name":"ล็อคข้อความทางลัด","note":"ปุ่มลัดเพื่อล็อคข้อความที่บันทึกไว้ในปัจจุบัน การล็อคข้อความที่บันทึกไว้หมายความว่าไม่สามารถเขียนทับหรือลบและจะไม่กู้คืนไปยังกล่องแชท Ctrl + คลิกที่ไอคอนข้อความที่บันทึกไว้จะสลับเป็นล็อค"},"clear_message_shortcut":{"name":"ล้างข้อความทางลัด","note":"แป้นพิมพ์ลัดเพื่อล้างกล่องโต้ตอบส่งข้อความขนาดใหญ่หรือลบข้อความที่บันทึกไว้เดิม กดอีกครั้งเพื่อเรียกคืนข้อความ"}}}},"vi":{"description":"Tốt hơn gửi tin nhắn lớn phiên bản 2. Mở một cửa sổ bật lên khi tin nhắn của bạn quá lớn, cho phép bạn tự động gửi tin nhắn dưới dạng một số tin nhắn nhỏ hơn. Chia tách thông điệp được tránh ở giữa các đoạn và câu.","toast_allsent_text":"Tất cả tin nhắn được gửi!","toast_restored_message_info":"Tin nhắn gốc được khôi phục.","toast_message_locked_info":"Đã lưu tin nhắn bị khóa!","toast_message_unlocked_info":"Lưu tin nhắn đã mở khóa!","toast_locked_message_warning":"Đã lưu tin nhắn bị khóa! Không thể mở Gửi đối thoại tin nhắn lớn!","toast_deleted_message_warning":"Tin nhắn đã lưu bị xóa!","toast_cleared_message_warning":"Tin nhắn đã được xóa!","toast_undeleted_message_info":"Tin nhắn được khôi phục!","toast_cannot_delete_warning":"Không thể xóa tin nhắn bị khóa!","modal_messages_translation":"Tin nhắn","modal_messages_warning":"Đừng gửi quá nhiều tin nhắn!","modal_load_warning":"Đã tải tin nhắn phù hợp!","modal_overwrite_warning":"Tin nhắn cuối cùng được ghi đè!","modal_header_text":"Gửi tin nhắn lớn","btn_cancel_text":"Hủy bỏ","btn_send_text":"Gửi","note_tooltip":"Mở tin nhắn","note_tooltip_locked":"Mở tin nhắn [bị khóa]","settings":{"main_settings":{"name":"Gửi cài đặt tin nhắn lớn","restore_message":{"name":"Khôi phục tin nhắn","note":"Có hay không khôi phục tin nhắn của bạn vào hộp trò chuyện khi bạn đóng hộp thoại Gửi tin nhắn lớn, được cắt bớt theo giới hạn tin nhắn.","options":{"always":"Luôn luôn","never":"Không bao giờ","short":"Đối với tin nhắn ngắn"}},"lock_by_default":{"name":"Khóa tin nhắn theo mặc định","note":"Khóa các tin nhắn đã lưu theo mặc định để ngăn chúng bị ghi đè hoặc bị xóa."},"note_on_right":{"name":"Hiển thị biểu tượng bên phải","note":"Hiển thị biểu tượng tin nhắn đã lưu ở bên phải."},"reduced_toasts":{"name":"Giảm thông báo","note":"Giảm số lượng thông báo bánh mì nướng."}},"shortcuts":{"name":"Gửi phím tắt tin nhắn lớn","open_modal_shortcut":{"name":"Gửi tin nhắn tắt","note":"Phím tắt để mở hộp thoại Gửi tin nhắn lớn."},"lock_message_shortcut":{"name":"Khóa tin nhắn","note":"Phím tắt để khóa tin nhắn đã lưu hiện tại. Khóa một tin nhắn đã lưu có nghĩa là nó không thể bị ghi đè hoặc bị xóa và sẽ không khôi phục vào hộp trò chuyện. Ctrl + Nhấp vào biểu tượng tin nhắn đã lưu cũng sẽ chuyển khóa."},"clear_message_shortcut":{"name":"Xóa tin nhắn tắt","note":"Phím tắt để xóa hộp thoại Gửi tin nhắn lớn hoặc xóa tin nhắn đã lưu hiện có. Nhấn một lần nữa để khôi phục tin nhắn."}}}}}};

    return !global.ZeresPluginLibrary ? class {
        constructor() {this._config = config;}
        getName() {return config.info.name;}
        getAuthor() {return config.info.authors.map(a => a.name).join(", ");}
        getDescription() {return config.info.description;}
        getVersion() {return config.info.version;}
        load() {
            const title = "Library Missing";
            const ModalStack = BdApi.findModuleByProps("push", "update", "pop", "popWithKey");
            const TextElement = BdApi.findModuleByProps("Sizes", "Weights");
            const ConfirmationModal = BdApi.findModule(m => m.defaultProps && m.key && m.key() == "confirm-modal");
            if (!ModalStack || !ConfirmationModal || !TextElement) return BdApi.alert(title, `The library plugin needed for ${config.info.name} is missing.<br /><br /> <a href="https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js" target="_blank">Click here to download the library!</a>`);
            ModalStack.push(function(props) {
                return BdApi.React.createElement(ConfirmationModal, Object.assign({
                    header: title,
                    children: [TextElement({color: TextElement.Colors.PRIMARY, children: [`The library plugin needed for ${config.info.name} is missing. Please click Download Now to install it.`]})],
                    red: false,
                    confirmText: "Download Now",
                    cancelText: "Cancel",
                    onConfirm: () => {
                        require("request").get("https://rauenzi.github.io/BDPluginLibrary/release/0PluginLibrary.plugin.js", async (error, response, body) => {
                            if (error) return require("electron").shell.openExternal("https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js");
                            await new Promise(r => require("fs").writeFile(require("path").join(ContentManager.pluginsFolder, "0PluginLibrary.plugin.js"), body, r));
                        });
                    }
                }, props));
            });
        }
        start() {}
        stop() {}
    } : (([Plugin, Api]) => {
        const plugin = (Plugin, Library) => {

    const {
      Logger, Patcher, Modals, DiscordClassModules, DiscordSelectors,
      DiscordModules, DOMTools, ReactTools, PluginUtilities, Toasts,
      WebpackModules, EmulatedTooltip
    } = Library;

    const ClassModules = {
      "Scrollbar": WebpackModules.getByProps('scrollbar'),
      "SizeLarge": WebpackModules.getByProps('sizeLarge'),
      "Horizontal": WebpackModules.getByProps('horizontal'),
      "InnerNoAutocomplete": WebpackModules.getByProps('innerNoAutocomplete')
    };

    const SEND_MESSAGE_HTML = `<span class="bslm-modal">
  <div class="bslm-modal-container">
    <div class="bslm-lock"><h4 id="lock">🔒</h4></div>
    <div id="inputtext-container" class="${ClassModules.InnerNoAutocomplete.inner} ${DiscordClassModules.Textarea.flex} ${DiscordClassModules.Textarea.innerNoAutocomplete} ${DiscordClassModules.BasicInputs.inputWrapper} ${ClassModules.SizeLarge.sizeLarge}">
    	  <textarea class="${DiscordClassModules.Textarea.textArea} ${ClassModules.Scrollbar.scrollbarGhostHairline}" id="modal-inputtext" placeholder="Message"></textarea>
    </div>
    <div class="${DiscordClassModules.Margins.marginTop4}" style="flex: 0 0 auto;">
      <h5 id="warning-message" class="${DiscordClassModules.Titles.h5 + DiscordClassModules.Titles.size12 + DiscordClassModules.Titles.height16 + DiscordClassModules.Titles.weightBold}"></h5>
      <h5 id="character-counter" class="${DiscordClassModules.Titles.h5 + DiscordClassModules.Titles.size12 + DiscordClassModules.Titles.height16 + DiscordClassModules.Titles.weightMedium}"></h5>
    </div>
  </div>
</span>
`;
    const CUSTOM_CSS = `.react-wrapper, .bslm-modal-container, .bslm-modal #inputtext-container {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  width: 100%;
  height: auto;
  min-height: none !important;
  max-height: none;
  flex: 1 1 auto;
}

.bslm-modal textarea {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  width: 100% !important;
  height: 100% !important;
  max-height: none !important;
  flex: 1 1 auto !important;
  resize: none;

}

.bslm-modal #lock {
  top: -2.3em;
  right: 1.6em;
  flex: 0 0 auto;
  position: absolute;
  margin: 0px;
  opacity: 0.25;
  visibility: hidden;
}

.bslm-modal #warning-message {
  color: #e00939;
  float: left;
  flex: 1 1 auto !important;
}

.bslm-modal #character-counter {
	float: right;
	color: white;
	opacity: 0.5;
  flex: 0 0 auto !important;
}

.message-note {
  bottom: -1.7em;
  position: absolute;
  stroke-opacity: 0.5;
  opacity: 0.5;

  transition: transform .2s ease-in-out, opacity .2s ease-in-out;
}

.message-note:hover {
  stroke-opacity: 1;
  opacity: 1;

  transform: scale(1.1);
}

.message-note button {
  background: transparent;
  padding: 0px;
}

.note-svg svg {
  padding: 2px;
  width: 16px !important;
  height: 16px !important;
}
`;
    const MESSAGE_NOTE_SVG = `<svg width="39.132mm" height="46.059mm" version="1.1" viewBox="0 0 39.132 46.059" xmlns="http://www.w3.org/2000/svg">
 <g transform="translate(-78.474 -95.423)" fill="none" stroke-linecap="round">
  <g stroke="#fff" stroke-width="5">
   <path d="m89.252 106.84h17.576"/>
   <path d="m89.252 117.26h17.576"/>
   <path d="m89.252 127.69h9.4785"/>
  </g>
  <path d="m106.79 139.23h-21.57c-2.4926 0-4.4994-2.0067-4.4994-4.4994v-32.56c0-2.4926 2.0067-4.4994 4.4994-4.4994h25.634c2.4926 0 4.4994 2.0067 4.4994 4.4994v21.505" stroke="#fffffe" stroke-linejoin="round" stroke-width="4.5" style="paint-order:markers fill stroke"/>
 </g>
</svg>
`;
    const MESSAGE_NOTELOCKED_SVG = `<svg width="39.132mm" height="46.059mm" version="1.1" viewBox="0 0 39.132 46.059" xmlns="http://www.w3.org/2000/svg">
 <g transform="translate(-78.474 -95.423)" fill="none" stroke-linecap="round">
  <g stroke="#fff" stroke-width="5">
   <path d="m89.252 106.84h17.576"/>
   <path d="m89.252 117.26h17.576"/>
   <path d="m89.252 127.69h9.4785"/>
  </g>
  <path d="m96.588 139.23h-11.365c-2.4926 0-4.4994-2.0067-4.4994-4.4994v-32.56c0-2.4926 2.0067-4.4994 4.4994-4.4994h25.634c2.4926 0 4.4994 2.0067 4.4994 4.4994v12.528" stroke="#fffffe" stroke-linejoin="round" stroke-width="4.5" style="paint-order:markers fill stroke"/>
 </g>
 <g stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round">
  <rect x="25.217" y="37.285" width="20.424" height="11.872" fill="#fcfcfc" stroke-width="3.8796" style="paint-order:markers fill stroke"/>
  <rect x="29.038" y="26.321" width="12.782" height="17.771" rx="6.391" ry="5.6176" fill="none" stroke-width="4.5" style="paint-order:markers fill stroke"/>
 </g>
</svg>
`;
    const MESSAGE_NOTE_HTML = `<div class="message-note" id="message-note">
  <button id="note-button">
    <div class="note-svg"></div>
  </button>
</div>
`;

    const MODAL_SIZE = Modals.ModalSizes.LARGE;

    const CHAR_LIMIT = 1900;
    const TOO_MANY_MESSAGES = 20;
    const MESSAGE_DELAY = 1000;
    const TIMEOUT = 200;
    const SHORT_TIMEOUT = 50;
    const WARNING_DISPLAY_TIME = 5000;
    const SAVE_PERIOD = 2500;

    const PARA_SEP = '\n';
    const SENTENCE_SEP = ' ';
    const SENTENCE_SEP_REGEX = new RegExp("(?<=[\.!?] *)"+SENTENCE_SEP, 'g');
    const WORD_SEP = ' ';

    const ENTER = 13;
    const ESC = 27;

    const KEY_CODE_ALT = 18;
    const KEY_CODE_CTRL = 17;
    const KEY_CODE_SHIFT = 16;
    const DISCORD_ALT = 164;
    const DISCORD_CTRL = 162;
    const DISCORD_SHIFT = 160;

    const ALWAYS = "always";
    const NEVER = "never";
    const FOR_SHORT_MESSAGES = "short";

    const CATEGORY = "category";
    const DROPDOWN = "dropdown";
    const VISIBLE = "visible";
    const HIDDEN = "hidden";
    const UNSET = "unset";

    const CHARCOUNTER_SELECTOR = '#charcounter';
    const MESSAGE_NOTE_SELECTOR = '.message-note';
    const TEXTAREA_TAG = "TEXTAREA";

    const DEFAULT_RIGHT_OFFSET = "0px";
    const CHARCOUNTER_RIGHT_PADDING = 5;

    const DEFAULT_SAVED_TEXT_STATE = {
      "savedText": "",
      "savedTextSelectionStart": 0,
      "savedTextSelectionEnd": 0,
      "savedTextScrollTop": 0,
      "messageLocked": false
    };

    return class BetterSendLargeMessages2 extends Plugin {
        constructor() {
          super();

          // State varaibles about chatbox and message box state
          this.savedText = "";
          this.savedTextSelectionStart = 0;
          this.savedTextSelectionEnd = 0;
          this.savedTextScrollTop = 0;
          this.savedChatText = "";
          this.savedChatSelectStart = 0;
          this.savedChatSelectEnd = 0;
          this.savedChatScrollTop = 0;
          this.deletedMessage = "";

          this.sending = false;
          this.keys = {};

          this.disableDefaultWarnings = false;
          this.savingPeriodically = false;

          this.messageLocked = false;
          this.messageLockedWarningShowed = false;
          this.messageLockedWarningShowing = false;
          this.deleteWarningToastShowing = false;

          this.rightOffset = DEFAULT_RIGHT_OFFSET;

          this.currentLocale = DiscordModules.LocaleManager.getLocale();

          // Hack because JS is shit
          this.onKeyDown = this.onKeyDown.bind(this);
          this.onKeyUp = this.onKeyUp.bind(this);
        }

        getDescription() {
          /*
            Update straggling locale strings (including description) when
            getting description
           */
          this.setRemainingLocale(DiscordModules.LocaleManager.getLocale());
          //return Plugin.prototype.getDescription.call(this); // call original getDescription function
          return super.getDescription();
        }

        onStart() {
            Logger.log("Started");

            PluginUtilities.addStyle(this.getName(), CUSTOM_CSS);

            this.loadMessageData();
            this.checkNote();

            this.setRemainingLocale(DiscordModules.LocaleManager.getLocale(), true);

            document.addEventListener("keydown", this.onKeyDown);
            document.addEventListener("keyup", this.onKeyUp);
        }

        onStop() {
            Logger.log("Stopped");
            Patcher.unpatchAll();
            PluginUtilities.removeStyle(this.getName());

            this.removeNoteButton();

            this.saveMessageData();

            this.savedText = "";
            this.savedChatText = "";
            this.deletedMessage = "";

            this.savingPeriodically = false;

            document.removeEventListener("keydown", this.onKeyDown);
            document.removeEventListener("keyup", this.onKeyUp);
        }

        getSettingsPanel() {
          const panel = this.buildSettingsPanel();
          let oldOnChange = panel.onChange;
          panel.onChange = () => {
            this.updateNotePosition();
            oldOnChange();
          };
          return panel.getElement();
        }

        observer(e) {
          if (!e.addedNodes.length || !e.type == "childList") return;
          if (e.target.tagName == TEXTAREA_TAG && e.target.placeholder != "") {
              let text = e.target.value;

              if (text.length > CHAR_LIMIT) {
                if (!this.messageLocked) {
                  this.showSendMessageModal(text);
                  this.setMessage("");
                } else if (!this.messageLockedWarningShowed && !this.messageLockedWarningShowing) {
                  Toasts.warning(this.strings.toast_locked_message_warning, {timeout: WARNING_DISPLAY_TIME});
                  this.messageLockedWarningShowing = true;
                  setTimeout(() => {this.messageLockedWarningShowing = false;}, WARNING_DISPLAY_TIME);
                }
                if (this.messageLockedWarningShowing) {
                  this.messageLockedWarningShowed = true;
                }
              } else {
                this.messageLockedWarningShowed = false;
              }
          }

          if (e.addedNodes[0] instanceof Element && e.addedNodes[0].querySelector(DiscordSelectors.Textarea.textArea)) {
            this.checkNote();
          }
        }

        // ------ Custom Functions ------
        showSendMessageModal(text, showWarning="") {
          /* Opens the send message modal, pre-filled with contents of text.
            Will always display text in the textarea unless it has a more up-to-date
            (longer) version saved, in which case it will display that instead
            (notifying the user).
           */
          let sendMessageModalElement = DOMTools.createElement(SEND_MESSAGE_HTML);
          let sendMessageModalReact = this.renderReactElements(sendMessageModalElement);

          if (this.savedText !== "" && this.subCompare(text, this.savedText) && this.savedText !== text) {
            showWarning = this.strings.modal_load_warning;
          } else if (text !== "") {
            if (this.savedText !== "" && !this.subCompare(this.savedText, text)) {
              showWarning = this.strings.modal_overwrite_warning;
            }
            if (this.settings.main_settings.lock_by_default && this.savedText !== text) {
              this.removeNoteButton(); //removed to trigger update/re-add later locked
              this.messageLocked = true;
            }
            this.savedText = text;
          }

          Modals.showModal(this.strings.modal_header_text, sendMessageModalReact, {
            confirmText: this.strings.btn_send_text,
            cancelText: this.strings.btn_cancel_text,
            onConfirm: () => {
              let messages = this.splitMessage(inputtext.value);

              this.sending = true;
              this.savedText = "";

              this.clearMessageData();
              this.checkNote();

              // Send all the messages
              messages.forEach((message,i) => {
                setTimeout(() => {
                  this.sendMessage(message);
                  if (i >= messages.length-1) {
                    Toasts.success(this.strings.toast_allsent_text);
                    this.sending = false;

                    if (this.savedChatText !== "") {
                      setTimeout(() => {
                        let textarea = this.chatTextbox();

                        this.restoreChatboxState(textarea);

                        if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_restored_message_info);
                      }, TIMEOUT);
                    }
                  }
                }, MESSAGE_DELAY * i);
              });
            },
            onCancel: () => {
              // If message is to be restored to chat box (not locked, no stored
              // chatbox message, no settings preventing it), just save over chatbox
              // state information with the inputtext (send large message box) Started
              // information then reload ('restore') the chatbox state
              if (
                !this.messageLocked && this.savedChatText === "" &&
                (this.settings.main_settings.restore_message === ALWAYS ||
                (this.settings.main_settings.restore_message === FOR_SHORT_MESSAGES && this.savedText.length <= CHAR_LIMIT))
              ) {
                let inputtext = this.sendMessageInputText();
                let scroll = this.savedChatScrollTop;

                this.saveChatboxState(inputtext);
                this.savedChatText = this.savedChatText.slice(0, CHAR_LIMIT);
                this.savedChatScrollTop = scroll;
                if (this.savedText === this.savedChatText) {
                  this.savedText = "";
                  this.clearMessageData();
                }
              }

              this.restoreChatboxState(chatbox);
              this.saveMessageData();
              this.checkNote();
            },
            size: MODAL_SIZE
          });

          // After modal pop up has loaded
          var inputtext = this.sendMessageInputText();
          var warning = DOMTools.Q('.bslm-modal #warning-message');
          var counter = DOMTools.Q('.bslm-modal #character-counter');
          let message_lock = DOMTools.Q('.bslm-lock #lock');

          let chatbox = this.chatTextbox();
          inputtext.placeholder = chatbox.placeholder;

          if (this.messageLocked) message_lock.style.visibility = "visible";
          else message_lock.style.visibility = "hidden";

          if (showWarning != "") {
            warning.innerText = showWarning;
            this.disableDefaultWarnings = true;
            setTimeout(() => {
              warning.innerText = "";
              this.disableDefaultWarnings = false;
            }, WARNING_DISPLAY_TIME);
          }

          // Submit and cancel shortcuts
          inputtext.addEventListener("keydown", (e) => {
      			if (e.ctrlKey && e.keyCode == ENTER) {
  				    DOMTools.Q('button[type=submit]').click();
      			}
    		  });
          inputtext.addEventListener("keydown", (e) => {
            if (e.keyCode == ESC) {
              inputtext.parents('form')[0].querySelector('button[type=button]').click();
            }
          });

          let updateCounter = () => {
            if (!this.sendMessageInputText()) return;

      			let length = inputtext.value.length;
      			let messageAmount = this.splitMessage(inputtext.value).length;
      			if (!this.disableDefaultWarnings) {warning.innerText = messageAmount > TOO_MANY_MESSAGES ? this.strings.modal_messages_warning : "";}
      			counter.innerText = length + " (" + (inputtext.selectionEnd - inputtext.selectionStart) + ") → " + this.strings.modal_messages_translation + ": " + messageAmount;

            if (
              this.settings.main_settings.lock_by_default &&
              this.savedText === "" &&
              inputtext.value !== ""
            ) {
              this.messageLocked = true;
            }

            this.saveMessageState(inputtext);

            this.checkNote();
            this.checkMessageLock();
          };

      		inputtext.addEventListener("keyup", (e) => {if (!(e.ctrlKey && e.keyCode == ENTER)) setTimeout(() => {updateCounter();}, SHORT_TIMEOUT);});
          inputtext.addEventListener("paste", () => {updateCounter();});
          inputtext.addEventListener("click", () => {updateCounter();});
          inputtext.addEventListener("scroll", () => {this.savedTextScrollTop = inputtext.scrollTop;});
          // Mouse click and drag detection logic borrowed from DevilBro
      		inputtext.addEventListener("mousedown", () => {
      			var mouseup = () => {
      				document.removeEventListener("mouseup", mouseup);
      				document.removeEventListener("mousemove", mousemove);
      			};
      			var mousemove = () => {
      				setTimeout(() => {updateCounter();}, SHORT_TIMEOUT);
      			};
      			document.addEventListener("mouseup", mouseup);
      			document.addEventListener("mousemove", mousemove);
      		});

          this.saveMessageData();
          if (!this.savingPeriodically) {
            this.savingPeriodically = true;
            var periodicSave = setInterval(() => {
              this.saveMessageData();
              if (!this.sendMessageInputText()) {
                this.savingPeriodically = false;
                clearInterval(periodicSave);
              }
            }, SAVE_PERIOD);
          }

          if (this.savedChatText === "" && chatbox.value !== "") {
            this.savedTextSelectionStart = chatbox.selectionStart;
            this.savedTextSelectionEnd = chatbox.selectionEnd;
          }

          this.restoreMessageState(inputtext);
          updateCounter();
        }

        sendMessage(text) {
          /*
            Send a single message to the currently open channel using the chatbox
           */
          let textarea = this.chatTextbox();
          if (textarea) {
            let press = new KeyboardEvent("keypress", {key: "Enter", code: "Enter", which: ENTER, keyCode: ENTER, bubbles: true});
            Object.defineProperties(press, {keyCode: {value: ENTER}, which: {value: ENTER}});

            this.setMessage(text);
            textarea.dispatchEvent(press);
          }
        }

        splitMessage(original_text) {
          /*
            Splits a large message up into several smaller messages to send individually.
            Message splitting is avoided in the middle of paragraphs or sentences
            if possible, but will default to splitting by words or characters
            if needed (on edge cases).
           */
          var messages = [];
          var text = original_text;

          // Split off next chunk as long as there is text to split
          while (text != "") {
            var vals = this.next_chunk(text.trim()); var chunk = vals[0]; text = vals[1];
            messages.push(chunk);
          }

          return messages;
        }

        saveChatboxState(chatTextbox) {
          /*
            Saves state information for the chatbox about `chatTextbox`
           */
          this.savedChatText = chatTextbox.value;
          this.savedChatSelectStart = chatTextbox.selectionStart;
          this.savedChatSelectEnd = chatTextbox.selectionEnd;
          this.savedChatScrollTop = chatTextbox.scrollTop;
        }

        restoreChatboxState(chatTextbox) {
          /*
            Restores state information for the chatbox about `chatTextbox`
           */
          this.setMessage(this.savedChatText);
          this.savedChatText = "";
          setTimeout(() => {
            chatTextbox.click();
            chatTextbox.focus();
            chatTextbox.selectionStart = this.savedChatSelectStart;
            chatTextbox.selectionEnd = this.savedChatSelectEnd;
            chatTextbox.scrollTop = this.savedChatScrollTop;
          }, TIMEOUT);
        }

        saveMessageState(inputtext) {
          /*
            Saves state information for the message box about `inputtext`
           */
          this.savedText = inputtext.value;
          this.savedTextSelectionStart = inputtext.selectionStart;
          this.savedTextSelectionEnd = inputtext.selectionEnd;
          this.savedTextScrollTop = inputtext.scrollTop;
        }

        restoreMessageState(inputtext) {
          /*
            Restores state information for the message box about `inputtext`
           */
          inputtext.value = this.savedText;
          inputtext.focus();
          inputtext.selectionStart = this.savedTextSelectionStart;
          inputtext.selectionEnd = this.savedTextSelectionEnd;
          inputtext.scrollTop = this.savedTextScrollTop;
        }

        checkMessageLock() {
          /*
            Check if the lock on the send large message dialogue should be showing
           */
          let message_lock = DOMTools.Q('.bslm-lock #lock');
          if (message_lock) {
            message_lock.style.visibility = this.messageLocked ? VISIBLE : HIDDEN;
          }
        }

        updateNotePosition() {
          /*
            Updates where the note should be positioned, taking into account
            the size of the character counter if the CharCounter plugin is
            detected.
           */
          let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
          let messageNote = DOMTools.Q(MESSAGE_NOTE_SELECTOR);
          this.rightOffset =  charcounter ? charcounter.width() + CHARCOUNTER_RIGHT_PADDING + "px" : DEFAULT_RIGHT_OFFSET;
          if (messageNote) messageNote.style.right = this.settings.main_settings.note_on_right ? this.rightOffset : UNSET;
        }

        checkNote() {
          /*
            Check if the note icon needs to be added or removed
           */
          if (this.savedText === "") {
            this.removeNoteButton();
          } else {
            this.addNoteButton();
          }

        }

        addNoteButton() {
          /*
            Add the note button icon below the chatbox.
           */
          let messageNote = DOMTools.Q('.message-note');
          if (messageNote) {
            let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
            this.updateNotePosition();
            return;
          }

          let note_svg = DOMTools.createElement(MESSAGE_NOTE_SVG)[0];
          let notelocked_svg = DOMTools.createElement(MESSAGE_NOTELOCKED_SVG)[0];
          let note = DOMTools.createElement(MESSAGE_NOTE_HTML)[0];
          if (this.settings.main_settings.note_on_right) note.style.right = this.rightOffset;
          let message_lock = DOMTools.Q('.bslm-lock #lock');
          let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
          let note_tooltip_text = "";

          if (this.messageLocked) {
            note.find('.note-svg').appendChild(notelocked_svg);
            note_tooltip_text = this.strings.note_tooltip_locked;
          } else {
            note.find('.note-svg').appendChild(note_svg);
            note_tooltip_text = this.strings.note_tooltip;
          }

          if (charcounter) {
            charcounter.addEventListener("DOMSubtreeModified", this.updateNotePosition.bind(this));
          }

          let note_tooltip = new EmulatedTooltip(note, note_tooltip_text);

          note.find('#note-button').addEventListener("click", (e) => {
            if (e.ctrlKey) {
              let message_lock = DOMTools.Q('.bslm-lock #lock');
              if (this.messageLocked) {
                this.messageLocked = false;
                note.find('.note-svg').removeChild(note.find('.note-svg').children[0]);
                note.find('.note-svg').appendChild(note_svg);
                note_tooltip.label = this.strings.note_tooltip;
                if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_message_unlocked_info);
              } else {
                this.messageLocked = true;
                note.find('.note-svg').removeChild(note.find('.note-svg').children[0]);
                note.find('.note-svg').appendChild(notelocked_svg);
                note_tooltip.label = this.strings.note_tooltip_locked;
                if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_message_locked_info);
              }
              this.saveMessageData();
              this.checkMessageLock();
            } else {
              this.saveChatboxState(this.chatTextbox());
              this.showSendMessageModal(this.savedText);
            }
          });

          DOMTools.Q(DiscordSelectors.Textarea.inner).appendChild(note);
          this.updateNotePosition();
        }

        removeNoteButton() {
          /*
            Remove the note button icon.
           */
          let note = DOMTools.Q('.message-note');
          let charcounter = DOMTools.Q(CHARCOUNTER_SELECTOR);
          if (note) note.parentNode.removeChild(note);
          if (charcounter) {
            charcounter.removeEventListener("DOMSubtreeModified", this.updateNotePosition.bind(this));
          }
          this.messageLocked = false;
        }

        shortcutPressed(shortcut) {
          /*
            Check if the key combination specified `shortcut` is currently being
            held down
           */
          for (let key of shortcut) {
            if (!this.keys[key]) {
              return false;
            }
          }

          for (let key in this.keys) {
            if (this.keys[key] && !shortcut.includes(parseInt(key))) {
              return false;
            }
          }

          return true;
        }

        onKeyDown(e) {
          /*
            onKeyDown event for the plugin to check when a set keyboard shortcut
            is being pressed.
           */
          if (!e.repeat) {
            this.setKey(e.keyCode, true);

            if (this.shortcutPressed(this.settings.shortcuts.open_modal_shortcut)) {
              this.showSendMessageModalShortcut();
              return;
            }

            if (this.shortcutPressed(this.settings.shortcuts.lock_message_shortcut)) {
              this.toggleMessageLockShortcut();
              return;
            }

            if (this.shortcutPressed(this.settings.shortcuts.clear_message_shortcut)) {
              this.clearMessageShortcut();
              return;
            }
          }
        }

        onKeyUp(e) {
          /*
            onKeyUp event for unsetting pressed keys.
           */
          this.setKey(e.keyCode, false);
        }

        clearMessageShortcut() {
          /*
            Executed when the clear message shortcut is pressed.
            Clears the message from the dialogue, if open, and delete saved message.
          */
          let inputtext = this.sendMessageInputText();
          if (this.savedText === "") {
            if (this.deletedMessage === "") return;
            this.savedText = this.deletedMessage;

            if (!this.settings.main_settings.reduced_toasts) Toasts.info(this.strings.toast_undeleted_message_info);
            if (inputtext) {
              inputtext.value = this.deletedMessage;
              inputtext.click();
            }
            this.deletedMessage = "";
            this.saveMessageData()
          } else if (!this.messageLocked) {
            this.deletedMessage = this.savedText;
            this.savedText = "";
            if (inputtext) {
              if (!this.settings.main_settings.reduced_toasts) Toasts.error(this.strings.toast_cleared_message_warning);
              inputtext.value = "";
              inputtext.click();
            } else {
              if (!this.settings.main_settings.reduced_toasts) Toasts.error(this.strings.toast_deleted_message_warning);
            }
            this.clearMessageData();
          } else {
            if (!this.deleteWarningToastShowing) {
              this.deleteWarningToastShowing = true;
              Toasts.warning(this.strings.toast_cannot_delete_warning, {timeout: WARNING_DISPLAY_TIME});
              setTimeout(() => {
                this.deleteWarningToastShowing = false;
              }, WARNING_DISPLAY_TIME);
            }
          }
          this.checkNote();
        }

        toggleMessageLockShortcut() {
          /*
            Execute when the toggle message lock shortcut is pressed.
            Toggles if the saved message is locked, meaning it cannot be overwritten
            or deleted.
           */
          let note = DOMTools.Q('.message-note');
          if (!note) return;

          let ctrl_click_event = new MouseEvent("click", {ctrlKey: true});
          note.find('#note-button').dispatchEvent(ctrl_click_event);
        }

        showSendMessageModalShortcut() {
          /*
            Executed when the show send message shortcut is pressed.
            Opens the "Send Large Message" dialogue box.
           */

          if (!this.sending && !DOMTools.Q('div[id="user-settings"]')) {
            let chatbox = this.chatTextbox();
            if (!chatbox) return;

            let inputtext = this.sendMessageInputText();
            if (!inputtext) {
              let text = chatbox.value;

              // Don't overwrite existing saved text if opening with shortcut
              if (this.savedText === "" || this.subCompare(this.savedText, text)) {
                this.savedChatScrollTop = chatbox.scrollTop;
                this.showSendMessageModal(text);
                this.setMessage("");
              } else {
                let warning = "";
                let clear_message = false;
                this.saveChatboxState(chatbox);
                if (this.subCompare(text, this.savedText)) { // `this.savedText` includes `text`
                  clear_message = true;
                  this.savedChatText = "";
                  warning = text === "" ? "" : this.strings.modal_load_warning;
                }
                this.showSendMessageModal(this.savedText, warning);
                if (clear_message) this.setMessage("");
              }
            } else {
              inputtext.parents('form')[0].querySelector('button[type=button]').click();
            }
          }
        }

        saveMessageData() {
          /*
            Saves all information about the saved message
           */
          let name = this.getName();
          let savedTextState = {
            "savedText": this.savedText,
            "savedTextSelectionStart": this.savedTextSelectionStart,
            "savedTextSelectionEnd": this.savedTextSelectionEnd,
            "savedTextScrollTop": this.savedTextScrollTop,
            "messageLocked": this.messageLocked
          };

          PluginUtilities.saveData(name, "savedTextState", savedTextState);
        }

        loadMessageData() {
          /*
            Loades existing saved message data, if it exists
           */
          let name = this.getName();

          let loadedSavedTextState = PluginUtilities.loadData(name, "savedTextState", DEFAULT_SAVED_TEXT_STATE);
          this.savedText = loadedSavedTextState['savedText'];
          this.savedTextSelectionStart = loadedSavedTextState['savedTextSelectionStart'];
          this.savedTextSelectionEnd = loadedSavedTextState['savedTextSelectionEnd'];
          this.savedTextScrollTop = loadedSavedTextState['savedTextScrollTop'];
          this.messageLocked = loadedSavedTextState['messageLocked'];
        }

        clearMessageData() {
          /*
            Clears the saved message data
           */
          let name = this.getName();
          PluginUtilities.saveData(name, "savedTextState", DEFAULT_SAVED_TEXT_STATE);
        }

        // ------ Custom Help Functions ------
        trim_by_chunks(chunks, separator, character_limit) {
          /*
            Trims off the largest number of chunks in `chunks` possible while
            keeping under `character_limit` when the chunks are concatinated
            together separated by serparator `separator`.
            */
          var prev = "";
          var curr = "";

          for (let i = 0; i < chunks.length; i++) {
            curr += chunks[i];

            if (curr.length > character_limit) {
              break;
            }

            prev = curr;
            curr += separator;
          }

          return [prev, chunks[0]];
        }

        next_chunk(full_text) {
          /*
            Gets the next largest possible chunks by largest chunk type from
            `full_text`.
            Finding the "largest chunk type" is considered as splitting by
            paragraph, then sentence, then word, then character.
            The first chunk type tested to have any number of chunks that are
            able to be included are group together and considered the "next chunk".
           */
          var text = full_text;
          var chunk = "";

          // Try splitting paragraph
          var paragraphs = text.split(PARA_SEP);
          var vals = this.trim_by_chunks(paragraphs, PARA_SEP, CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          if (chunk != "") {
            return [chunk, full_text.replace(chunk, "")];
          }

          // Try splitting by sentence
          var sentences = text.split(SENTENCE_SEP_REGEX);
          var vals = this.trim_by_chunks(sentences, SENTENCE_SEP, CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          if (chunk != "") {
            return [chunk, full_text.replace(chunk, "")];
          }

          // Try splitting by word
          var words = text.split(WORD_SEP);
          var vals = this.trim_by_chunks(words, WORD_SEP, CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          if (chunk != "") {
            return [chunk, full_text.replace(chunk, "")];
          }

          // Try split by character
          var vals = this.trim_by_chunks(text, "", CHAR_LIMIT); chunk = vals[0]; text = vals[1];

          return [chunk, full_text.replace(chunk, "")];
        }

        chatTextbox() {
          /*
            Returns the chatbox's textarea DOM element.
           */
          return DOMTools.Q('form').querySelector(DiscordSelectors.Textarea.textArea);
        }

        sendMessageInputText() {
          return DOMTools.Q('.bslm-modal #modal-inputtext');
        }

        subCompare(string1, string2) {
          /*
            Compares if string1 is a substring of string2
          */
          let str1 = string1;
          let str2 = string2;

          let char_limit = str1.length;

          str1 = str1.slice(0, char_limit);
          str2 = str2.slice(0, char_limit);

          return str1 == str2;
        }

        setKey(key, set) {
          /*
            Sets a key as being held down (`set` = true) or not held down
            (`set` = false) by key code, ensuring the Disord key codes are used
            for control characters.
           */
          switch (key) {
            case KEY_CODE_ALT:
              this.keys[DISCORD_ALT] = set;
              break;
            case KEY_CODE_CTRL:
              this.keys[DISCORD_CTRL] = set;
              break;
            case KEY_CODE_SHIFT:
              this.keys[DISCORD_SHIFT] = set;
              break;
            default:
              this.keys[key] = set;
              break;
          }
        }

        setMessage(text) {
          /*
            Set the message that is shown in the chatbox textarea.
           */
          let textarea = this.chatTextbox();
          let form = textarea.parents('form')[0];
          let formOwnerInstance = ReactTools.getOwnerInstance(form);
          formOwnerInstance.setState({textValue:text});
        }

        renderReactElements(elements) {
          /*
            Converts a list of elements into a list of rendered react elements.
          */
          var reactElements = [];

          if (!elements.length) elements = [elements];
          elements.forEach((element) => {
            if (element.nodeName != "#text") {
              reactElements.push(ReactTools.createWrappedElement(element));
            }
          });

          return reactElements;
        }

        setRemainingLocale(locale, force=false) {
          /*
            Sets the remainig localisation strings for the plugin that are not
            supported automatically by zlib.
            This includes updating the description, group names,
            and dropdown labels.
            Involves digging access to the this._config property of the Plugin class
          */
          if (this.currentLocale === locale && !force) return;
          this.currentLocale = locale;

          let strings = this._config.strings;
          if (!strings.hasOwnProperty(locale)) locale = "en";
          let settings = strings[locale].settings;
          let defaultConfig = this._config.defaultConfig;

          // Set description
          this._config.info.description = strings[locale].description;

          // Set names for groups and labels for dropdowns
          for (let i = 0; i < defaultConfig.length; i++) {
            let defaultSetting = defaultConfig[i];

            if (defaultSetting.type === DROPDOWN) {
              let defaultOptions = defaultSetting.options;
              for (let j = 0; j < defaultOptions.length; j++) {
                this._config.defaultConfig[i].options[j].label =
                  settings[defaultSetting.id].options[defaultOptions[j].value];
              }
            } else if (defaultSetting.type === CATEGORY) {
              this._config.defaultConfig[i].name = settings[defaultSetting.id].name;
              let categorySettings = defaultSetting.settings;
              for (let k = 0; k < categorySettings.length; k++) {
                let categorySetting = categorySettings[k];
                if (categorySetting.type === DROPDOWN) {
                  let defaultOptions = categorySetting.options;
                  for (let j = 0; j < defaultOptions.length; j++) {
                    this._config.defaultConfig[i].settings[k].options[j].label =
                      settings[defaultSetting.id][categorySetting.id].options[defaultOptions[j].value];
                  }
                }
              }
            }
          }
        }
    };
};
        return plugin(Plugin, Api);
    })(global.ZeresPluginLibrary.buildPlugin(config));
})();
/*@end@*/